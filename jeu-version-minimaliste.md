# Le jeu de la dette relationnelle, version minimaliste, serious game rapide.

## Pourquoi ce jeu ?
Comprendre l'impacte des tensions non traitées dans les dynamiques de groupe.

La version minimaliste se concentre sur le contexte professionnel.

Ce jeu vous aidera à réfléchir aux questions suivantes :
- Au cours de votre vie, à quoi ont ressemblé les dynamiques de groupe des équipes dont vous avez fait partie ? et quelle a été votre part ?
- Qu'est-ce qui distingue une équipe efficace et durable d'une autre équipe ?
- De quoi un turn-over important est-il symptomatique ?
- Qu'est-ce qui mène au burn-out et comment s'en prémunir ?

## Public, durée et materiel nécessaire

- 2 joueurs ou plus (à 200 ça marche aussi)
- à partir de 8 ans au doigt mouillé
- Durée conseillée 1h :
  - (5 minutes) Présentation initiale & règles du jeu
  - (10min) 5 tours chronométrés, de 2 minutes chacun
  - (5min) débrief intermédiaire
  - (10min) 5 tours chronométrés, de 2 minutes chacun
  - (5min) débrief intermédiaire
  - (10min) 5 tours chronométrés, de 2 minutes chacun
  - (15min) débrief final

Materiel :
- pour chaque joueur :
  - 1 feuille (ou un post-it)
  - 1 stylo
  - 1 trombone
  - 1 D4 (dé à 4 faces)
  - 1 D6 (dé à 6 faces)
  - 1 D8 (...)
  - 1 D10
  - 1 D12
- 1 feuille (ou un post-it) par équipe
- Le récap des règles imprimé pour chaque équipe voir chaque joueur (dont la table de formation initiale).

## But
**Maximiser la valeur produite en tant qu'équipe.**
**Être l'équipe qui à produit le plus (s'il y a plusieurs équipes)**


## Règles du jeu

### Mise en place initiale
- Répartissez-vous dans une ou plusieurs équipes. *Essayer d'avoir des équipes de taille différentes pour vous rendre compte de l'effet du nombre sur les dynamiques de groupe. Exemple : à 10, une équipe de 3 et une équipe de 7.*
- Chaque équipe note sur une feuille :
  - Titre : Feuille d'équipe (éventuellement un nom pour l'équipe)
  - Produit : une coche par point de production, un changement de colonne par tour.
  - Consommé : indiquez le nombre de personnes dans l'équipe
  - Burn-out : s'il y a burn-out, rajoutez une coche à chaque nouveau burn-out.
- Chaque joueur prépare sa feuille/post-it :
    - Ajoutez une échelle de 1 à 12 en bord de feuille
    - Placez le curseur de tension à 1 (trombone rouge)

### Déroulement d'un tour de jeu
1. L'animataire lance un chronomètre de 2 minutes, à la fin du compte à rebours, toute action non terminée est remplacé par une tension pour le joueur. (vous pouvez vous concerter pour choisir quoi faire, mais passez à l'action rapidement).
2. Chaque personne choisie/déclare son action du tour.
3. Tout le monde effectue son action et en applique les effets. En cas d'échec, placez votre dé au centre pour que le groupe puisse compter le nombre de contagions à résoudre.
5. Résolvez les contagions (un jet de sauvegarde par dé au centre, en ignorant vos propres dé)
6. **Besoin de fonctionnement :** Retranchez à la production de votre équipe 1 par joueurs de votre équipe (l'auto-consomation/salaire/besoin de fonctionnement). Ce qui reste est votre production effective du tour. Si elle est négative, chaque membre de l'équipe augmente d'un niveau de tension (sauf ceux en arrêt maladie).
7. Le joueur qui a la feuille d'équipe ajoute la production effective du tour à celle des tours précédents. Si le total est négatif, la partie est perdu. Débriefez et recommencez à zero.


### Résoudre une action au dé
Pour produire, avec ou sans dépassement de soi, et se former, vous allez résoudre votre action au dé. Lancez votre dé, éventuellement ceux de dépassement de soi. Pour chaque dé lancé, retranchez-y votre niveau de tension pour déterminer votre marge de réussite. Si elle est inférieur ou égal zero, c'est un échec : l'action prévue n'a pas lieu, votre niveau de tension monte de 1, et vous placez votre dé au centre pour que les autres joueurs de votre équipe effectue un "jet de sauvegarde" pour déterminer s'ils sont affecté ou non.

### Jet de sauvegarde
Lorsque vous devez faire un jet de sauvegarde, lancez votre dé, si le résultat est supérieur à votre niveau de tension, vous n'êtes pas affecté. Sinon vos tensions augmentent d'1 niveau (résolvez tout les jets de sauvegarde avant de mettre à jour votre niveau de tension).

### Le burn-out
Si votre niveau de tension atteint le nombre de face de votre dé, vous n'êtes plus en mesure de grand chose, c'est le burn-out : vous débloquez automatiquement la compétence "Arrêt maladie" qui va vous permettre de prendre soin de vous, mais vous sortez abimé de ce burn-out : Rétrogradez à un dé ayant 2 faces en moins que votre dé actuel.
Tant que votre niveau de tension est supérieur ou égale au nombre de face de votre dé, vous êtes considéré comme en burn-out.


### Les compétences
#### Produire (toujours possible)
Lancez votre dé, retranchez votre niveau de tension au résultat du dé pour déterminer votre production.
Si elle est supérieur à 0, ajoutez autant de coche "Produit" sur votre feuille d'équipe.
Sinon, augmentez d'un niveau de tension et déclenchez une contagion de tension dans votre équipe.

#### Dépassement de soi (toujours possible)
Ego, café, coke, peu importe votre secret, quand il y a besoin de vous, vous savez vous surpasser !
Après votre jet de dé, vous pouvez tenter de vous surpasser si le résultat ne vous conviens pas.
Cela n'annulera pas les tensions généré par vos échec ni leur contagion aux autres, mais cela vous donne de nouvelles chances de réussir votre action.
Lancez un dé à 2 face de plus accompagné d'un dé à 2 face de moins.
Gardez le plus haut score de tout vos jet du tour pour réussir votre action, et appliquez tous les échecs (tension et contagions).
NOTE : Si vous lanciez déjà un D12, vous pouvez le relancer jusqu'à 2 fois au lieu de lancer des dé différents.
Exemple : A ce tour, la production d'un groupe de 3 repose sur vous. Vous lancez votre D6 et faite 4, mais vous aviez 2 de tension, donc vous produiriez seulement 2 contre 3 nécessaire pour le tour. Vous choisissez de lancer un D4 et un D8 en plus. Vous faite 2 et 5. Vous atteignez votre objectif de production de justesse, mais générez un échec et la tension associé. Vous auriez aussi pu échouer votre premier jet en faisant 1, Vous surpasser et faire 2 et 3, et finir avec 1 de production pour 3 nécessaire, et 2 points de tension + contagions.

#### Se former (toujours possible)
3 types de formations sont disponnible :
- performance (remplacez votre dé par un dé ayant 2 faces de plus D6 -> D8, D8 -> D10...)
- soutien (vous pourrez aider une personne de votre équipe à se sentir mieux en lui apportant de l'`écoute active`, son niveau de tension redessendra au votre)
- syndicale (vous serez en mesure de vous mettre en `arrêt maladie` sans attendre d'être en burn-out)

Se former est par essence un dépassement de soi, lancez les 3 dés vous correspondant (au départ D4, D6, D8) et traitez les résultats comme un dépassement de soi.

#### Arrêt maladie
Passez votre tour et descendez vos tensions d'un niveau (minimum 1), aucune nouvelle tension ne peux vous atteindre (pas de contagion de tension ni de tension par manque de production d'équipe).

Attention : Les membres d'une équipe ne peuvent pas être tous en arrêt maladie simultanément. (sinon vous perdez la partie)

#### Écoute active
Écoutant et écouté y consacre leur tour.
L'écouté ramène son niveau de tension à celui de l'écoutant.



## Licences
Le simulateur en ligne est sous licence [AGPL-3.0](https://choosealicense.com/licenses/agpl-3.0/) ou supérieur.

Les règles du jeu et le materiel destiné à l'impression est sous licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) ou supérieur.
