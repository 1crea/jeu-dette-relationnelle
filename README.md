# Nous(é)moi ~ Le jeu de la dette relationnelle
En'jeu commun
Ecologie du stress
Economie du stress
Sans rancœur !
Sans rancune !

## Pourquoi ce jeu ?
Pour tout type de raisons, les êtres humains ont tendance à former des groupes.
Au sein de ces groupes ont lieu des interactions, et certaines d'entre elles ne servent pas la raison d'être qui vous a amené à ce groupe. Des grains de sables, des tensions s'accumulent au fil du temps, et parfois mène le groupe à se disloquer. D'autres fois pourtant le groupe perdure et prospère.
Ce jeu vous propose de décoder les mécaniques qui mènent à chacune de ces issues et d'intégrer la notion de dette relationnelle dans vos sphères pour être en mesure de la rembourser quand le besoin s'en fait sentir.

## Public, durée et materiel nécessaire

- 2 joueurs ou plus (à 200 ça marche aussi)
- à partir de 8 ans au doigt mouillé
- Durée 30 minutes ou plus selon le nombre de manches/itérations (et le nombre de joueur)

Materiel à minima :
  - papier et stylo (pour chaque joueur)
  - une application pour simuler des lancers de dé à N faces.

Materiel idéal :
  - Une feuille personnage par joueur
  - Un D4, D6, D8, D10 et D12 (D12 = dé à 12 faces) par personne
  - Une feuille d'équipe par équipe
  - Une carte de chaque compétence par personne
  - Une pile de cartes événement
  - Une pile de cartes scénario
  - 3 à 5 jetons rouges (stress) par personne
  - 3 à 5 jetons verts (apaisement) par personne
  - 3 à 5 jetons jaunes (contagion de stress) par personne
  - 3 à 5 jetons bleus (production) par personne
  - 5 à 15 jetons cyans (production non polluante)
  - 5 à 15 jetons blancs (production régénératrice)
  - un trombone rouge (pour marquer le niveau de stress) par personne
  - un trombone noir (pour indiquer le type de dé) par personne
  - un trombone bleu (de production effective) par équipe
  - un trombone gris (de pollution) pour la partie

## But
**Produire un maximum de valeur en tant qu'équipe.** 
Selon la raison d'être de votre équipe, la valeur produite peut être de l'action politique, de l'épanouissement affectif, des biens et services professionnels ou domestiques...

- **Mode express** : en 5 tours
- **Mode itératif** : en 3 manches de 5 tours chacune avec un débrief à la fin de chaque manche
- **Mode à vie** : en 60 tours toutes variantes activées

Testez le [simulateur en ligne](https://1crea.frama.io/jeu-dette-relationnelle/demo.html) pour vous faire une idée des mécaniques de jeu si vous n'avez personne avec qui jouer.

### Condition de victoire
- survivre au moins 5 tours et totaliser 100 points de production effective en tant qu'équipe

### Condition de défaite
- Tous les membres de votre équipe sont en burn-out
- La production effective de votre équipe, cumulée depuis le début de la partie passe en dessous de zero.

## Règles du jeu

### Mise en place initiale
- Répartissez-vous dans une ou plusieurs équipes. *Essayer d'avoir des équipes de taille différentes pour vous rendre compte de l'effet du nombre sur les dynamiques de groupe. Exemple : à 10, une équipe de 3 et une équipe de 7.*
- Chaque équipe prend une fiche scénario.
- Chaque joueur commence avec :
  - Une fiche personnage
  - Le curseur de stress (trombone rouge) placé sur 1. Il pourra augmenter puis descendre, mais jamais en dessous de 1.
  - Le curseur faces de dé (trombone noir) placé sur 6. Il représente votre expertise (à produire, à prendre soin, à supporter le stress, à intégrer des formations...)
  - Une carte Produire
  - Une carte Enseigner
  - Une carte Apprendre
  - Une autre carte compétence tirée au hasard parmi celles suggérées sur la fiche scénario de votre équipe. Cette carte représente votre formation initiale qui vous a mené jusqu'à cette équipe.

### Déroulement d'un tour de jeu
1. L'animataire tire une carte événement pour le tour et en lis les effets.
2. Chaque personne choisie parmi ses cartes action ce qu'elle compte faire ce tour et pose la carte retenue devant elle (ou les cartes en cas de compétence multi-tache).
3. Une fois toutes les actions choisies, lancez les dés nécessaires à résoudre ces actions. Pour toutes les réussites, placez sur votre fiche personnage à la perpendiculaire les cartes compétences enseignées ou en acquisition. Placez les jetons de production sur votre fiche d'équipe. Pour les échecs, placer un jeton stress (rouge) sur votre fiche personnage et un jeton contagion (jaune) sur chacune des fiches personnage des autres concernés par votre action.


4. parmi celles à sa disposition (produire, transmettre, se former, utiliser une compétence spéciale)
3. lance un D8 pour déterminer la formation externe disponible ce tour-ci. Tous les joueurs qui le souhaitent peuvent utiliser leur action du tour pour tenter de s'y former.
2. 
3. Une fois toutes les actions choisies, lancez les dés nécessaires à résoudre ces actions. Les effets ne s'appliqueront qu'au changement de tour, qu'ils soient bénéfique ou non (notez les pour penser à les appliquer au changement de tour).
4. **Besoin de fonctionnement :** Totalisez la production de votre équipe ce tour et retranchez-lui 1 par joueurs de votre équipe. Ce qui reste est votre production effective du tour. Si elle est négative, chaque membre de l'équipe augmente d'un niveau de stress.
5. Le joueur qui a la feuille d'équipe ajoute la production effective du tour à celle des tours précédents.

-----
5. 
    - *Toujours disponible* apporter du score à son équipe : lancez votre dé¹, si vous réussissez, ajoutez son résultat + bonus/malus éventuels au score de votre équipe.
    - se former : choisissez une des formations découvertes, lancez votre dé¹, si vous réussissez, appliquez-vous l'effet de la formation.
    - *Disponible après formation ou burn-out* prendre soin de soi (vacances, thérapie...) : retirez 1 à votre niveau de stress personnel sans avoir à lancer son dé
    - *Autres selon formations*

### Résoudre une action au dé
À la phase de résolution des actions, sauf mention contraire, lancez votre dé. Si votre score est inférieur ou égal à votre niveau de stress, c'est un échec : l'action prévue n'a pas lieu, votre niveau de stress monte de 1 (au changement de tour), et chaque autre joueur impliqué¹ effectue un "jet de sauvegarde à {votre niveau de stress}" pour tenter d'éviter d'être stressé par contagion.

¹ Est considéré comme impliqué : 
- Pour une action de production, les membres de votre équipe qui produisent également ce tour-ci
- Pour une formation les personnes qui transmettent et reçoivent cette formation
- Les personnes concernées par un soin (qu'elles le prodiguent ou en bénéficient)

### Jet de sauvegarde
Lorsque vous devez faire un jet de sauvegarde, lancez votre dé, si le résultat est supérieur à votre niveau de stress, vous avez réussi à éviter la contagion de stress. Sinon votre stress augmente d'1 niveau (au changement de tour).



### Le burn-out
Si votre niveau de stress atteint le nombre de face de votre dé, vous ne pouvez plus réussir aucune action, c'est le burn-out : vous débloquez automatiquement la compétence "Zen" qui va vous permettre de prendre soin de vous, mais vous sortez abimé de ce burn-out : Rétrogradez à un dé ayant 2 faces en moins que votre dé actuel.
Tant que votre niveau de stress est supérieur ou égale au nombre de face de votre dé, vous êtes considéré comme en burn-out.

### Table de formation initiale
1. Formation pédagogie : `pédagogie du quotidien` + `transmission facilitée`.
2. Formation Permaculture : augmentez votre experitse de 2 + `soutien communautaire/chantier participatif` (consomez 3 de prod pour lancer un D20 pour vos jets du tour prochain)
3. Formation Agile & Lean : augmentez votre expertise de 2 + compétence `variable d'ajustement` (vous êtes au service du groupe, attendez que les autres ai agit pour déterminer votre action (avec 2 faces de moins))
4. Formation syndicale : félicitation, si ça ne va pas, vous osez vous mettre en `arrêt maladie` pour récupérer. Pas besoin d'atteindre le burn-out pour ça. + passif : jet de sauvegarde pour vos propres échecs
5. Baggage en CNV : vous pouvez prendre soin d'une des personnes de votre équipe avec la compétence `écoute active`. + 1 esquive de contagion par tour pour soi.
6. Approches restauratives, systémique, dialogique... Vous savez aider les groupes à traverser leurs conflits avec la compétence `thérapie collective`. + `gardien du coeur`
7. Gouvernance partagé : D8 + multi-tache
8. Le département RH n'a pas bien cerné les besoins de l'équipe. Votre formation initiale ne vous sert à rien ici. Votre niveau de stress monte à 2.

### Compétences
#### Produire (toujours possible)
Lancez votre dé, retranchez votre niveau de tension au résultat du dé pour déterminer votre production.
Si elle est supérieur à 0, ajoutez autant de coche "Produit" sur votre feuille d'équipe.
Sinon, augmentez d'un niveau de tension et déclenchez une contagion de tension dans votre équipe.

#### Dépassement de soi (toujours possible)
Ego, café, coke, peu importe votre secret, quand il y a besoin de vous, vous savez vous surpasser !
Après votre jet de dé, vous pouvez tenter de vous surpasser si le résultat ne vous conviens pas.
Cela n'annulera pas les tensions généré par vos échec ni leur contagion aux autres, mais cela vous donne de nouvelles chances de réussir votre action.
Lancez un dé à 2 face de plus accompagné d'un dé à 2 face de moins.
Gardez le plus haut socre de tout vos jet du tour pour réussir votre action, et appliquez tous les échecs (tension et contagions).
NOTE : Si vous lanciez déjà un D12, vous pouvez le relancer jusqu'à 2 fois au lieu de lancer des dé différents.
Exemple : A ce tour, la production d'un groupe de 3 repose sur vous. Vous lancez votre D6 et faite 4, mais vous aviez 2 de tension, donc vous produiriez seulement 2 contre 3 nécessaire pour le tour. Vous choisissez de lancer un D4 et un D8 en plus. Vous faite 2 et 5. Vous atteignez votre objectif de production de justesse, mais générez un échec et la tension associé. Vous auriez aussi pu échouer votre premier jet en faisant 1, Vous surpasser et faire 2 et 3, et finir avec 1 de production pour 3 nécessaire, et 2 points de tension + contagions.

#### Transmettre (toujours possible)
Vous tentez de transmettre une de vos compétences à un apprennant. Vous y dédiez votre tour ainsi que votre apprennant. Vous devez réussir tout deux votre action pour que la transmission soit réussie. 

#### Se former (toujours possible)
Vous tentez d'apprendre une compétence, soit en interne (voir `Transmettre`, `Transmission facilitée` ou `Pédagogie du quotidien`) soit en externe.
Dans tout les cas, lancez votre dé pour voir si vous réussissez à intégrer cet apprentissage (vous devez faire plus que votre niveau de stress).

Les formations externe coutent 1 (de prod accumulé lors des tours précédents) lors du tour ou elles sont tirées en carte événement.
Lors des tours suivant, il reste possible de s'y former, mais en commandant la formation pour 3 + 2 par participant.

#### Arrêt maladie
Si votre tension et superieur à la moitié de votre expertise, vous pouvez vous mettre en arrêt maladie.
Dans ce cas, aucun nouvelle tension ne peux vous atteindre (pas de contagion de tension ni de tension de manque de production d'équipe).
Vous abaissez votre tension d'un niveau gratuitement.

Vous pouvez bénéficier d'un accompagnement psy plus ou moins intensif pour décendre plus rapidement jusqu'à la moitié de votre expertise.

| cout | tension libéré |
| ----:|:------------- |
|    0 | 1             |
|    1 | 2             |
|    3 | 3             |
|    6 | 4             |
|   10 | 5             |
|   15 | 6             |

Le cout est retranché à la production d'équipe accumulée.

Synergie :
Si vous avez également CNV & cercle, vous pouvez descendre jusqu'à 2 de tension avec cette compétence (mais son activation nécessite toujours d'avoir une tension superieur à la moitié de votre expertise)


#### Soin collectif / cercle 
En préparation, le nombre de point de réussite crédite le nombre de joueur pouvant participer au cercle au tour prochain (et suivant s'il reste des points).
Au moins une personne formée doit participer au cercle.
Il suffit d'une réussite durant le cercle pour que les échecs ne génèrent pas de stress (sauf échec suplémentaire en dépassement de soi).
Les participants baissent d'autant de stress que de réussite par les participants.

#### Gardien du coeur (passif/réactif)
Vous veillez sur une personne de votre choix (autre que vous) : confiez lui votre carte "gardien du coeur".
Si votre protégé doit prendre un niveau de stress, elle retourne la carte gardien du coeur à la place (1 fois par tour).
En début de tour, retournez la carte pour qu'elle soit de nouveau utilisable. Vous pouvez également la confier à quelqu'un d'autre.

#### écoute active
L'écoutant lance son dé. L'écoutant retranche son propre stress au résultat et s'il est positif le retranche au stress de l'écouté (sinon l'écoutant prend 1 de stress et contagion à l'écouté). L'écouté lance alors son dé, retranche son stress (qui à déjà pu baisser grace à l'écoute) et retranche à son tour le résultat à son propre niveau de stress s'il est positif (si c'est un échec, il prend 1 de stress et contagione l'écoutant).

#### Pédagogie du quotidien (passif)
Il est désormais possible d'apprendre à votre contact sans que vous vous y consacriez à plein temps.
Si une personne se forme auprès de vous, vous lancerez ce tour-ci un dé avec 2 faces de moins, et vos échec et ceux de votre apprenti vous impactent mutuellement (contagion) ainsi que ceux qui font la même activité que vous.

## Transmission facilitée
Vous pouvez vous dédier à transmettre une compétence à une ou plusieurs personne.
Si vous le faite, votre nvieau de réussite (résultat-stress) pourra servire de coup de pouce pour que vos élèves réussissent leur apprentissage.
Si un élève rate sont jet d'apprentissage, vous pouvez compléter en coup de pouce jusqu'à en faire une réussite. Si vous avez plusieurs élèves, libre à vous de choisir qui vous ratrappez en priorité.


### Cartes compétences
1. Formation Agile & Lean, changez votre dé pour un dé ayant 2 faces de plus. (Chaque membre de l'équipe ne peu faire cette formation qu'une fois par carte de ce type découverte par l'équipe)
2. Formation Holacratie & Sociocratie, changez votre dé pour un dé ayant 2 faces de plus. (Chaque membre de l'équipe ne peu faire cette formation qu'une fois par carte de ce type découverte par l'équipe)
3. Formation Yoga & Méditation : félicitation, vous n'avez pas besoin d'un burn-out pour débloquer l'action "prendre soin de soi".
4. Formation CNV : vous pouvez prendre soin d'une des personnes de votre équipe. lancez votre dé¹, si vous réussissez, réduisez le niveau de stress dula bénéficiaire comme suit : retranchez son niveau de stress à votre résultat de dé pour descendre d'un niveau son stress. Réitérez jusqu'à avoir épuisé le résultat de votre dé. Le reste est perdu. Exemple : Vous faire 8 au dé, pour apaiser quelqu'un dont le niveau de stress est 4. Vous dépensez 4 pour le descendre à 3, il vous reste 4, vous dépensez 3 pour le descendre à 2, il vous reste 1, il vous faudrais 2 pour le descendre à 1, votre action s'arrête donc là.
  Si vous échouez un jet de sauvegarde, vous pouvez en faire un 2nd. Vous n'augmenterez d'un niveau de stress que si le second échoue également.
6. Formation aux cercles restauratifs : Il faut au moins une personne formée aux cercles dans votre équipe pour pouvoir en appeler un. Être formé aux cercles restauratifs vous permettra, en cas d'échec, un 2nd essai pour chacune de vos actions "cercle restauratifs" ou préparation du cercle.
  Pour qu'un cercle restauratif ai lieu, il faut qu'au moins une personne souhaite en appeler un. Pour cela, les personnes le souhaitant vont consacrer leur action du tour à préparer un cercle. Il suffit qu'une des personnes préparant le cercle réussisse son action pour que le cercle ai lieu le tour suivant. Les échecs en préparation de cercle ont les éffets habituels en matière de stress et de contagion. Au tour suivant, toutes les personnes de l'équipe qui souhaitent consacrer leur action du tour à participer au cercle peuvent le faire (avec un minimum de 4, sauf si l'équipe à moins de 4 membres). Pour ce faire, elles lancent leur dé. Pour chaque réussite, le niveau de stress des participants baisse d'un niveau. Les échecs sont sans effet sauf si aucun participant ne réussi, dans ce cas tous les participants montent d'un niveau en stress, et le reste de l'équipe doit réussir un jet de sauvegarde pour ne pas faire de même. Le cercle peut se poursuivre de tour en tour tant qu'au moins 4 personne y participent chaque tour et qu'au moins une réussie son jet de dé.

7. Formation Pédagogie : Vous pouvez enseigner à plusieurs personnes une de vos compétences spéciales. De plus, lorsque vous transmettez, vous pouvez relancer votre dé en cas d'échec.
8. Formation Permaculture, changez votre dé pour un dé ayant 2 faces de plus. (Chaque membre de l'équipe ne peu faire cette formation qu'une fois par carte de ce type découverte par l'équipe)

9. Formation Auto-gestion, stigmergie, intelligence collective, co-organisation, co-responsabilité & auto-responsabilité, changez votre dé pour un dé ayant 2 faces de plus. (Chaque membre de l'équipe ne peu faire cette formation qu'une fois par carte de ce type découverte par l'équipe)
10. Formation Multi-tache : Félicitation, désormais au lieu de faire une chose bien vous pouvez tenter de faire plusieurs choses moins bien en subdivisant votre dé entre plusieurs actions. Par exemple au lieu de faire une action en lançant un D12, vous pouvez en faire deux, une avec un D4 et l'autre avec un D8 ou deux avec un D6 chacune, voir 3 avec un D4 chacune. Vous risquez les contagions de stress dans chacune de vos actions et vos échec contagionne dans chacune de vos actions.
11. Formation Variable d'ajustement : Vous pouvez pour le tour utiliser un dé avec 2 face de moins mais choisir votre action du tour après que tout le monde ai jetté ses dé (donc en sachant qui à réussi ou non). En faisant ça, vous êtes considéré comme impliqué dans toutes les actions de votre équipes que ce soit pour recevoir du stress ou en transmettre en cas d'échec de votre action.
12. Formation Planification : Vous pouvez passer votre tour à planifier le tour suivant pour vous et éventuellement d'autres personnes du groupe. Les concernés annoncent leur action et ne pourront en changer que la planification réussisse ou non. En cas de réussite, ajoutez aux concernés, pour l'action prévue de leur prochain tour, un mix au choix des planificateurs : 2 face de dé supplémentaire par réussite ou un bonus de score en cas de réussite égale au niveau de réussite en planification (résultat du dé-niveau de stress). En cas d'échec, ajoutez un niveau de stress au planificateur et aux bénéficiaires et retirez 2 faces aux dés des bénéficiaires.

#### Compétences spéciales
- **Optimisation fiscale** (équipe Start-up CleanPhone) :
- **Radicaliser** (équipe Green-Blocks) :
- DARK : souffre douleur : Si quelqu'un échoue un jet, il peut vous donner son jeton de stress (rouge) en prendre un jeton jaune à la place. Spécial : Quand vous partez en burn-out tiez au dé une autre pesonne de l'équipe, transférez lui la compétence souffre douleur (vous ne l'aurez plus en sortie de burn-out)
- DARK : Caïd / chef / oppressif / pervers narcisique : quand vous êtes sensé prendre un jeton rouge, vous pouvez à la place donner un jeton jaune à chacun des membre de votre équipe.

### Cartes Scénario
En début de partie, chaque équipe tire une carte scénario qui indiquera ses conditions de victoire et de défaite.

- **Eco-village rainbow :** Votre objectif est de vivre simplement, en harmonie avec la nature. Pour gagner, tous vos membres doivent être formés à la permaculture, aux cercles restauratifs, à la CNV, et à la méditation. Aucun de vos membres ne doit faire de burn-out, sinon c'est la défaite immédiate pour votre équipe. Compétences initiales suggérées : 3 cartes méditation, 1 carte CNV, 1 carte CR, 1 carte Permaculture, 1 Holacratie/Sociocratie, 1 carte Agile/Lean, 1 carte Pédagogie, 1 carte multi-tache
- **Start-up CleanPhone :** Votre objectif est d'améliorer la qualité de vie des gens. Pour cela, vous avez choisi de concevoir le smartphone du futur ! Votre marque de fabrique, sécurité et vie privée, votre smartphone sera aussi respectueux de ses possesseurs que possible. Pour gagner la partie, vous devez avec 100 points de production effective cumulés, créer le prototype de votre smartphone du futur et le présenter dans les salons de la Hi-tech au 4 coin du monde ! Bon, il parait qu'à certaines étapes, ça pollue, mais depuis vos locaux climatisés, ça ne se voit pas. À chaque tour, divisez votre production effective par votre rendement écologique (1 initialement), arrondissez au supérieur et voila la pollution que vous avez produite ce tour-ci. En bonus, vous pouvez apprendre en formation extérieure la compétence spéciale **Optimisation fiscale** qui vous permet, si une personne de votre équipe effectue cette action et la réussie de puiser dans votre production effective des tours précédents pour combler le manque de production de ce tour sans générer de stress.
- **Start-up Agro+ :** Votre objectif est de nourrir le monde ! Pour cela, vous devez produire un maximum de nourriture. 100 point de production effective cumulée et s'est gagné ! Mais personne n'est parfait : à chaque tour, divisez votre production effective par votre rendement écologique (1 initialement), arrondissez au supérieur et voila la pollution que vous avez produite ce tour-ci... Sauf que vous pouvez aussi produire de manière non polluante, voir régénératrice : la formation permaculture vous donne aussi les compétences spéciales :
    - production non-polluante : quand vous lancez votre dé, si vous réussissez, divisez le résultat par deux, arrondi au supérieur, cette production-là n'engendre pas de pollution du tout. (mais attention à produire suffisamment pour ne pas stresser et finir en burn-out)
    - production régénératrice : quand vous lancez votre dé, si vous réussissez, divisez le résultat par trois, arrondi à l'inférieur, pour chaque point de production ainsi produit, retirez un point de pollution (même à une autre équipe si vous voulez).
- **Think-tank écologique :** Votre objectif, réduire la pollution sans vous mettre la société à dos. Pour gagner la partie, la pollution produite doit baisser de tour en tour, enfin augmenter moins vite, enfin si la pollution produite lors des 2 derniers tours est inférieure à celle des 2 premiers tours, c'est déjà bien. Pour cela, vous devez produire ce que vous savez le mieux faire, des recommandations, des labels, des audits et grâce à votre travail, l'équipe start-up deviendra plus écolo : tous les 10 points de production effective de votre équipe leur rendement écologique augmentera d'un point.
- **Les Green-blocks :** Votre objectif, zero pollution, ASAP ! Si aucune pollution n'a été générée au cours des 3 derniers tours, vous gagnez la partie. Pour cela, vous devez produire de l'action directe : sabotez la production des capitalistes et stressez-les suffisamment pour leur faire passer l'envie de continuer. Retranchez vos points de production effective à ceux d'une des équipes start-up. S'ils passent en négatifs, leur stress augmentera d'1 comme s'ils n'avaient pas produit suffisamment, et vous devrez faire deux jets de sauvegarde pour ne pas vous faire prendre, pour chaque échec, votre niveau de stress augmentera d'1 point. Bonus : les membres de votre équipe ont automatiquement la compétence spéciale **Radicaliser** pour recruter ceux que le système a broyés. Lancez votre dé, si vous réussissez, choisissez dans n'importe quelle équipe une personne ayant fait un burn-out, si elle échoue son jet de sauvegarde, elle rejoindra votre équipe au changement de tour.
- **État providence :** Votre objectif, c'est le bien commun... et la ré-élection. Pour gagner, aucune équipe ne doit perdre, et vous devez montrer à l'opinion publique que vous faites bien votre travail en produisant 50 points de propagande, sachant que pour chaque personne en état de burn-out toutes équipes confondues, à chaque tour, vous perdez 1 de production. Sachant également que la moitié de la pollution (arrondie à l'inférieure) produite durant votre mandat (la partie) sera décompté de vos points de propagande. Sachant légalement que vous pouvez utiliser vos points de production pour baisser celle de n'importe quelle autre équipe afin d'éviter qu'elle n'en fasse perdre une autre ou qu'elle ne pollue de trop, mais vous ne pouvez pas baisser leur production effective en dessous de zero. Enfin, quand un pédagogue de votre équipe enseigne une de ses compétences, vous pouvez choisir d'ouvrir sa formation en externe pour que les autres équipes, quelles qu'elles soient, puissent en bénéficier.

## Fiches
### Joueur
- Niveau de stress actuel :
- Dé à X faces :
- Bonus/malus :
#### Complément variantes :
- Dé soin à X faces :
- Bonus d'acienneté :
- Bonus d'xp soin :

- Trésorerie :
- Bonus genré :
- Seuils de handicapes : (résultat de chaque jet de dé de handicap)
- Personnes à charges : (resultat du dé initial + tour de début et fin de periode de charge)

### Equipe
| N° du tour | Nbr équipiers | Score du tour | Score cumulé |
| ---------- | ------------- | ------------- | ------------ |
|            |               |               |              |

## Variantes :

#### Se former (action de base)
Version minimaliste/facile : Vous pouvez utiliser votre action du tour pour tenter d'apprendre n'importe quelle compétence. Annoncez la compétence visée, lancez votre dé, retranchez-y votre niveau de stress, si le résultat est supérieur à 0, vous avez aquis la compétence. Sinon c'est un échec, augmentez votre niveau de stress de 1 et déclenchez une contagion de stress.

#### Dépassement de soi
Avant de lancer votre dé (D6 par exemple), vous pouvez choisir d'en lancer 3 à la place : votre dé ainsi que ceux ayant + et - 2 faces (1D4, 1D6 et 1D8 dans notre exemple). Gardez le meilleur score pour réussir l'action, mais appliquez les conséquences de stress des 3 dés. Exemple : Pour produire, au lieu de lancer un D6, vous pouvez lancer 1D4, 1D6 et 1D8, vous faites 1, 6 et 3, votre niveau de stress était à 3, vous montez à 5 et déclenchez 2 contagions de stress, mais vous réussissez également à produire 6.
Si vous avez également la compétence "prendre soin de soin/arrêt maladie", vous pouvez choisir de ne lancer que les 2 dés extrème (pas celui du milieu)

#### Agile & Lean, Permaculture, Holacratie & Sociocratie
*Compétence passive (son effet est permanent)* Pour chaque formation de ce type que vous effectuez, changez de dé pour un dé ayant 2 faces de plus. (D6 -> D8, D8 -> D10, D10 -> D12)
 
Variante difficile : L'effet est temporaire, bonus pour 3 tours après la première formation, pour 6 tours après la 2ème, pour 12 tours après la 3ème, pour 24 après la 4ème, x2 à chaque fois.

#### Arrêt Maladie
Si votre stress dépasse la moitié du nombre de faces de votre dé (D6 : 3, D12 : 6), vous pouvez utiliser cette compétence pour prendre soin de vous. Dans ce cas, pas besoin de lancer votre dé, vous réussissez automatiquement à descendre votre stress d'un niveau. Vous êtes également protégé des contagions de stress. Si votre équipe ne produit pas assez, au lieu de prendre automatiquement 1 point de stress, vous pouvez faire un jet de sauvegarde pour tenter d'esquiver ce stress.
**Suivi psy / Thérapie individuelle** est une compétence passive qui vous permet de prendre soin de vous (arrêt maladie/vacances/retraite spirituelle) si votre stress dépasse le quart arrondi au supérieur du nombre de faces de votre dé (D4:1, D6:2, D8:2, D10:3, D12:3). Elle complète la formation syndicale et son arrêt maladie. Si vous n'avez que l'un des deux, vous ne pouvez descendre à qu'à la moitié de votre stress maximum.
Les compétences *CNV/écoute active* et *thérapie collective* abaissent chacune de 1 le seuil de stress où vous pouvez utiliser la compétence *prendre soin de soi/arrêt maladie/vacances/retraite spirituelle*.

#### CNV/écoute active
Choisissez quelqu'un (d'autre que vous) qui bénéficiera de votre écoute. Lancez votre dé, retranchez-y votre niveau de stress. Si le résultat est supérieur à 0, déduisez ce montant du niveau de stress dula bénéficiaire. Sinon, vous prenez un de stress et déclenchez une contagion de stress.

#### Thérapie collective / approche restorative, systémique...
Vous pouvez tenter de *préparer un cercle / une thérapie collective* (plusieurs peuvent le faire, il suffit qu'une personne réussisse pour que le cercle/la thérapie ai lieu au tour suivant) (les échec en préparation génère de stresse et de la contagion comme d'habitude).
Si la préparation a réussi, au tour suivant, toutes les personnes qui le souhaitent, qu'elles soient formées ou non, peuvent, comme action du tour, participer au cercle/thérapie. Elles lancent leur dé et pour chaque réussite (> à leur niveau de stress) l'ensemble des participant⋅e⋅s baisse d'un niveau de stress. Si au moins une personne formée réussie, il est possible de poursuivre le cercle au tour suivant avec qui le souhaite (celà peut être des personnes qui n'ont pas participé au tour précédent) et celà de tour en tour (cela économise un nouveau tour de préparation). Si aucun participant ne réussi son jet de dé, c'est un échec collectif : tous les participants prennent 1 niveau de stress, et le reste de l'équipe doit faire un jet de sauvegarde pour se protéger de la contagion.



### Vices et addictions
Chaque joueur à une jauge d'addiction à 1 initialement.
En consommant autant de points de production effective des tours précédents que votre niveau d'addiction, vous pouvez réduire temporairement pour le tour votre niveau de stress de deux niveaux, mais en perdant également pour le tour 2 face à votre dé.
Vous pouvez utiliser cette mécanique plusieurs fois pour le tour pour baisser davantage votre niveau de stress.
Pour chaque tour où vous l'utilisez, augmenter la jauge d'addiction d'un.
Pour chaque tour où vous vous en passez, faite un jet de sauvegarde pour baisser la jauge d'addiction d'un. Ce jet de sauvegarde se réussi ou s'echoue non pas selon votre niveau de stress seulement, mais selon votre niveau de stress cumulé à votre niveau d'addiction. Si vous le réussissez, baissez votre niveau d'addiction de moitié, arrondi au supérieur. Si vous l'échouez, augmentez votre niveau de stress d'1 et votre niveau d'addiction reste le même.

### Table des cartes événements
1. Pandémie majeur, tout le monde augmente son niveau de stress de 2.
2. Crise économique, fasse à un avenir incertain, tout le monde augmente son niveau de stress de 1.
3. Dècès d'un parent, tirez au hasard un membre de votre équipe, son stress augmente de 2.
4. Cambriolage, tirez au hasard un membre de votre équipe, son stress augmente de 2.
5. Pneu creuvé, tirez au hasard un membre de votre équipe, son stress augmente de 1.
6. Découverte passionnante, tirez au hasard un membre de votre équipe, son dé progresse d'un seuil.
7. Fin d'une periode stressante, tout le monde baisse d'un niveau de stress (stress minimum : 1)


### Précarité financière
à chaque tour dans une équipe vous gagnez 1 + 1 de prime si vous faite le max de votre dé, + 1 si votre bonus d'ancienneté est superieur ou égale à +4 (un mode de gouvernance peux être utilisé pour en décider autrement, par exemple, le chef peux décider de se payer mieux. Dans tout les cas, les salaire sont déduit du score d'équipe à chaque tour). Que vous soyez dans une équipe ou non, à chaque tour, vous dépensez 1 pour vivre et 1 par personne à charge et par handicap / maladie chronique. En début de partie, vous tirez un dé 4 -1 personnes à charge. Vous pouvez choisir d'échelloner leur charge à partir du tour que vous voulez entre le 1er et le 20ème. Chaque personne restera à charge 20 tour. Tout les 10 tours (à partir du tour 10), vous tirez un dé 6, et notez le résultat. Si votre zone de stress inclue le résultat du dé alors vous serez handicapé tant que votre zone de stress ne redescent pas assez. Ce qui veux dire qu'au pire, au 30ième tour vous pouvez cumuler 3 personnes à charges + 3 handicapes, soit un coup de la vie de 7/tour. La zone de stress ne pouvant descendre en dessous de 1, tout dé de handicape à 1 signifie un handicape à vie. Enfin, un joueur ayant une zone de stress au maximum aura ses frais de handicapes pris en charge en puisant dans le score de l'équipe où il a atteint ce seuil (arrêt maladie/dépression/burn-out) jusqu'a ce que son niveau de stress baisse.
### Monde sexiste
en début de partie, jettez un dé 10, pair, vous êtes un homme, impair, vous êtes une femme, 1 aucun de ces groupe ne vous inclue, vous cumulez les inconvénient des deux sans avoir les avantages, vous serez désigné comme trans. Les hommes commencent avec un dé 4 +2, les femmes avec un dé 6, les trans avec un dé 4. Si vous utilisez aussi la variante précarité financière, les homme gagne 1 de plus par tour dans une équipe, les trans lance un dé de handicape dès le premier tour.
### Ignare
Impossible de prendre soin avant qu'il n'y ai eu au moins un burn-out, à partir de là, possible de prendre soin à l'aveugle qui ressois le soin est alétoire ou répartie également entre tout le groupe, mais égallement possible de se former pour identifier de qui prendre soin (pouvoir cibler les personnes ayant un niveau de stress de 4&+, en 1 tour, formation refaisable pour cibler les 3&+, puis les 2&+)
### Gouvernance : Chaque équipe décide d'un ou plusierus modes de gouvernance pour changer les mode de gouvernance, recruter, exclure et décider de qui fait quoi dans l'équipe. exemples mais vous pouvez imaginer d'autres choses :
    - hierarchique : un chef se désigne, il décidera pour l'équipe.
    - vote à la majorité : chaque décision n'est validée que si plus de la moitié sont pour.
    - consentement ou droit de véto : chaque décision n'est validée que si personne n'est contre.
    - auto-détermination : chacun décide pour soi-même, personne ne peut le contester.
    - règle des 3 pirates : si 2 autres personnes sont d'accord alors vas-y.
    - pile ou face : lance un dé ou une pièce, si le résultat est pair ou face c'est validé

### Ancienneté :
Chaque tour dans une équipe, vous prennez un jeton (ou faite un trait sur votre feuille).
Dès que vous avez le nombre de jeton correspondant au bonus à atteindre, consomez-les pour passer au bonus d'ancienneté suivant. Par exemple, si vous avez 0 d'ancienneté, à la fin du tour, vous gagnez un jeton que vous pouvez convertir pour passer à +1 d'ancienneté. Si vous avez +3 d'ancienneté, il vous faut 4 jetons à consomer pour passer à +4 d'ancienneté.

#### XP soin
Pour chaque action de soin réussie, vous prennez un jeton.
Pour chaque echec de soin, gagnez autant de jeton que le nombre de face de votre dé - votre niveau de stress (0 si ça devait être négatif).
Vous gagnez des bonus de soin de la même manière que les bonus d'ancienneté : dès que vous avez suffisement de jeton pour le bonus suivant, consomez-les pour débloquer le bonus.


## Idées à tester :
- Quand un jet de dé est inférieur ou égal à son seuil de stress, au lieu d'augmenter d'1 son seuil, on l'augmente d'1+seuil de stress - score au dé, mais peut-être avec une croissance du seuil de stress similaire à l'experience, sauf que du coup ça risque de revenir au même mais en plus complexe.
- Pour qu'il y ai un intérêt à faire des équipes de + de 2 personnes, soit l'objectif est de faire le plus gros score d'équipe et non le plus gros score global, mais du coup des organisations en sous équipe fédéré ne peuvent plus être représenté, soit les scores entre membre d'une même équipe se multiplient entre eux (donc au moins fumble, on tombe à 0) soit on additionne mais applique un multiplicateur en fonction du nombre de personnes dans l'équipe x n ou x 1,n voir un racine de (score x n), mais on s'éloigne de quelquechose de calculable de tête et donc jouable sans ordi.

## Licences
Le simulateur en ligne est sous licence [AGPL-3.0](https://choosealicense.com/licenses/agpl-3.0/) ou supérieur.

Les règles du jeu et le materiel destiné à l'impression est sous licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) ou supérieur.

## Sur une thématique proche :
- [Stressss!!!](https://nicotondeur.fr/index.php/2021/02/09/stressss-v2-0-en-creative-commons/)
