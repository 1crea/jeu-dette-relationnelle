- Chaque joueur commence au D10, sans tensions, avec un niveau de stress correspondant au plus petit résultat de 2 lancé de D10.
- Faire un 0 au dé est toujours un échec.
- Chaque équipe doit produire au moins autant que son nombre de joueur à chaque tour. Pour chaque point manquant, chaque joueur pioche un point de tension avec quelqu'un qui à échoué son action ce tour s'il y a, un autre équipier sinon. Si vous avez le choix, prenez toujours un jeton de tension à l'égard de la personne avec qui vous êtes déjà le plus en tension. (si vous lui en vouliez déjà, c'est plus facile de lae considérer responsable de ce qui ne vous va pas).

- Pour produire il faut lancer un dé, le résultat du dé est votre score de production, sous réserve que vous fassiez davantage que la sommes de toutes vos tensions avec vos coéquipiers de production du tour/jour.

