import {PubSub} from "./PubSub.mjs";
import {GameSimulator} from "./GameSimulator.mjs";
import {useAI} from "./gameVariants.mjs";

export class Runner extends PubSub{
    constructor(rounds) {
        super();
        this.rounds = [];
        for(let game = 0;game<rounds;game++) this.rounds.push(new GameSimulator([/*1,2,3,*/4,5,6,7,8,9,10,20,30,40,50]))
        useAI(1)
        for(let game = 0;game<rounds;game++) this.rounds.push(new GameSimulator([/*1,2,3,*/4,5,6,7,8,9,10,20,30,40,50]))
        useAI(3)
        for(let game = 0;game<rounds;game++) this.rounds.push(new GameSimulator([/*1,2,3,*/4,5,6,7,8,9,10,20,30,40,50]))
        useAI(4)
        for(let game = 0;game<rounds;game++) this.rounds.push(new GameSimulator([/*1,2,3,*/4,5,6,7,8,9,10,20,30,40,50]))
        this.rounds.forEach(g=>g.subscribe(this))
    }
    run(turn){
        for(let i=1;i<=turn;i++) this.rounds.forEach(game=>game.play(1));
    }
}