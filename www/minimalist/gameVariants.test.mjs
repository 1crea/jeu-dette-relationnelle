import test from 'ava';
import * as app from './gameVariants.mjs';

test('getRules() return default rules (tension.min)',t => t.is(app.getRules().tension.min,1));
test('getRules() return default rules (cleverness.init)',t => t.is(app.getRules().cleverness.init,6));
test('getRules(1) return first alternatives rules (cleverness.init)',t => t.is(app.getRules(1).cleverness.init,8));
test('getRules(1) return first alternatives rules (tension.min)',t => t.is(app.getRules(1).tension.min,1));
test('getRules(2) return alternative rules (cleverness.init)',t => t.is(app.getRules(2).cleverness.init,12));
test('getRules(2) return alternative rules (cleverness.min)',t => t.is(app.getRules(2).cleverness.min,4));
test('activeRules is default rules',t =>{
    t.is(app.activeRules.cleverness.init,6)
});
test('useRules(2) change activeRules to 2nd alt',t =>{
    app.useRules(2)
    t.is(app.activeRules.cleverness.init,12)
});
test('getRules() still return default rules (cleverness.init)',t => t.is(app.getRules().cleverness.init,6));
test('getAI() return default AI (chooseTeamActions)',t => t.is(typeof app.getAI().chooseTeamActions,'function'));
test('getAI() return default AI (chooseTeamBoosts)',t => t.is(typeof app.getAI().chooseTeamBoosts,'function'));
test('getAI(1) return different AI than default',t => t.not(app.getAI(1).chooseTeamActions, app.getAI().chooseTeamActions));
test('activeAI is default AI',t =>{
    t.is(app.activeAI.chooseTeamActions,app.getAI().chooseTeamActions)
});
test('useAI(1) change activeAI to 1st alt (OPNB)',t =>{
    t.is(app.activeAI.name,'defaultAI')
    app.useAI(1)
    t.is(app.activeAI.name,'OPNB')
});
