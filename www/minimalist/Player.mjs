import {activeRules} from "./gameVariants.mjs";
import {rollDice} from "./utils.mjs";
import {Burnout, Fail, Prod, Recovery} from "./events.mjs";
import {PubSub} from "./PubSub.mjs";

export class Player extends PubSub{
    constructor(rules=undefined) {
        super()
        this.gameVariant = rules || activeRules;
        this.cleverness = this.gameVariant.cleverness.init
        this.tension = this.gameVariant.tension.init
        this.pending = {heal:0,tension:0,spread:0,actions:[]};
        this.immune = {tension:0,spread:0}
        this.learned = {heal:0, selfCare: 0,cleverness:0};
        this.turn = 1;
        this.log = [];
        this.history = '';
        this.turnHistory = `#${this.turn}${this.status()}`;
    }
    produce(){
        this.log.push('produce');
        const prod = rollDice(this.cleverness,this.tension);
        this.turnHistory+=`P${prod>0?prod:'F'}`;
        this.produced = prod>0?prod:0;
        if(prod<=0) return this.fail();

        this.publish(new Prod(this,prod));

    }
    boost(){
        this.log.push('boost');
        const smallDice = rollDice(Math.max(this.cleverness-2,this.gameVariant.cleverness.min),this.tension);
        const bigDice = rollDice(Math.min(this.cleverness+2,this.gameVariant.cleverness.max),this.tension);

        let extra = 0;
        switch(this.log[this.log.length-2]){
            case 'produce':
                if(smallDice<=0) this.fail();
                if(bigDice<=0) this.fail();

                extra = Math.max(0, smallDice-this.produced, bigDice-this.produced);
                this.turnHistory+=`B${this.produced>0?'+':''}${extra}${smallDice<=0?'F':''}${bigDice<=0?'F':''}`;
                if(extra>0) this.publish(new Prod(this,extra));
                break;
            case 'learnSelfCare':
                this.turnHistory+=`B${this.learn('selfCare',smallDice)+this.learn('selfCare',bigDice)}`;
                break;
            case 'learnHeal':
                this.turnHistory+=`B${this.learn('heal',smallDice)+this.learn('heal',bigDice)}`;
                break;
            case 'learnCleverness':
                this.turnHistory+=`B${this.learn('cleverness',smallDice)+this.learn('cleverness',bigDice)}`;
                break;
        }

    }
    learn(skillName,diceResult){
        if(diceResult<=0) this.fail();
        const learnConditions = skillName==='cleverness'?this.gameVariant[skillName].learn[`fromLvl${Math.min(this.cleverness,this.gameVariant.cleverness.max-2)}`]:this.gameVariant[skillName].learn;
        const success = diceResult >= learnConditions.minimalDiceScore;
        if(success) this.learned[skillName]++;
        const forHistory=`${success>0?`${this.learned[skillName]}/${learnConditions.successCount}`:''}${diceResult<=0?'F':''}`;
        if(this.learned[skillName]>=learnConditions.successCount) this.learned[skillName] = true;
        if(skillName==='cleverness' && this.learned.cleverness===true){
            this.learned.cleverness = 0;
            this.cleverness+=2;
        }
        return forHistory;
    }
    learnHeal(){
        this.log.push('learnHeal');
        this.turnHistory+=`LH${this.learn('heal', rollDice(this.cleverness,this.tension))}`;
    }
    learnSelfCare(){
        this.log.push('learnSelfCare');
        this.turnHistory+=`LSC${this.learn('selfCare', rollDice(this.cleverness,this.tension))}`;
    }
    learnCleverness(){
        this.log.push('learnCleverness');
        this.turnHistory+=`LC${this.learn('cleverness', rollDice(this.cleverness,this.tension))}`;
    }
    canBoost(){
        if(this.turn<this.gameVariant.boost.autoUnlockTurn) return false;
        if(this.log[this.log.length-1]==='selfCare') return false;
        if(this.log[this.log.length-1]==='heal') return false;
        if(this.log[this.log.length-1]==='healed') return false;
        return true;
    }
    canHeal(){
        return true===this.learned.heal;
    }
    canSelfCare(){
        if(this.tension<=this.gameVariant.tension.min) return false;
        return true===this.learned.selfCare;
    }
    canLearn(skill){
        if(this.turn<this.gameVariant[skill].learn.unlockTurn) return false;
        if(skill==='cleverness') return this.cleverness<this.gameVariant.cleverness.max;
        return !(true===this.learned[skill]);
    }
    canLearnCleverness(){
        return this.canLearn('cleverness');
    }
    canLearnHeal(){
        return this.canLearn('heal');
    }
    canLearnSelfCare(){
        return this.canLearn('selfCare');
    }
    fail(){
        this.pending.tension += this.gameVariant.fail.cost.tension;
        this.publish(new Fail(this));
    }
    heal(targetToHeal){
        const healAmount = Math.max(0, targetToHeal.tension - this.tension);
        targetToHeal.pending.heal += healAmount;
        this.log.push('heal');
        this.turnHistory+=`H${healAmount}`;
        targetToHeal.log.push('healed');
        targetToHeal.turnHistory+=`Hed${healAmount}`;
    }
    healed(){
        this.immune.tension = this.gameVariant.healed.immune.tension;
    }
    selfCare(){
        this.log.push('selfCare');
        this.turnHistory+=`SC`;
        this.immune.tension=this.gameVariant.selfCare.immune.tension;
        this.pending.heal+=this.gameVariant.selfCare.heal.tension;
    }
    setAction(actionName){
        this.pending.actions.push(actionName);
    }
    setBoost(active=true){
        if(active) this.pending.actions.push('boost');
    }
    executePendingAction(){
        const actionName = this.pending.actions.shift();
        if(!actionName) return;
        if(typeof actionName === 'string') return this[actionName]();
        const name = actionName.shift();
        this[name](...actionName);
    }
    resolveSpread(){
        while (this.pending.spread>0){
            this.pending.spread--
            if(rollDice(this.cleverness, this.tension) <= 0) this.pending.tension++;
        }
    }
    applyHeal(){
        this.tension= Math.max(this.tension-this.pending.heal, this.gameVariant.tension.min);
        this.pending.heal = 0;
        if(this.tension<this.cleverness && this.inBurnout) {
            this.inBurnout=false;
            this.publish(new Recovery(this));
        }
    }
    applyTension(){
        if(this.immune.tension===true) this.pending.tension = 0;
        if(this.turn<=this.gameVariant.easyStart.endOfMercy) this.pending.tension = Math.min(this.pending.tension, this.gameVariant.easyStart.maxPending.tension)
        this.tension += this.pending.tension;
        this.pending.tension = 0;
        if(this.tension>=this.cleverness && !this.inBurnout) this.burnout();
    }
    burnout(){
        this.cleverness = Math.max(this.cleverness-this.gameVariant.burnout.cost.cleverness,this.gameVariant.cleverness.min);
        this.learned.selfCare = true;
        this.inBurnout = true;
        this.publish(new Burnout(this));
    }
    endOfTurn(teamProdEnough= true){
        this.resolveSpread();
        this.applyHeal();
        if(!teamProdEnough) this.pending.tension += this.gameVariant.insufficientTeamProd.cost.tension;
        this.applyTension();
        this.immune.tension=0;
        this.immune.spread=0;
        this.history+= this.turnHistory;
        this.turn++;
        this.turnHistory = `#${this.turn}${this.status()}`

    }

    status(){
        return `[C${this.cleverness}T${this.tension}${this.learned.heal===true?'H':''}${this.learned.selfCare===true?'SC':''}]`;
    }
}