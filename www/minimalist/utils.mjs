export function rollDice(cleverness, tension){
    if(!staticFunc.random) staticFunc.random = Math.random;
    return Math.floor(1+staticFunc.random()*cleverness-tension);
}
const staticFunc = {};
export {staticFunc};
