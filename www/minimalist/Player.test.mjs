import test from 'ava';
import * as app from './Player.mjs';
import {activeRules, getRules} from './gameVariants.mjs';
import {staticFunc as mock} from "./utils.mjs";
test('i can create new Player()',t => t.truthy(new app.Player()));
test('a new Player() have activeRules.tension.init as tension',t =>{
    t.is((new app.Player()).tension, activeRules.tension.init);
});
test('player canBoost or not depending of autoUnlockTurn.',t=>{
    const p = new app.Player();
    p.produce();
    t.is(p.canBoost(),false);
    p.endOfTurn();
    t.is(p.canBoost(),true);
});
test('player canBoost in gameVariant 4',t=>{
    const p = new app.Player(getRules(4));
    p.produce();
    t.is(p.canBoost(),true);
});
test('player canBoost after produce but not after selfCare or heal.',t=>{
    const p = new app.Player();
    p.endOfTurn();
    p.produce();
    t.is(p.canBoost(),true);
    p.endOfTurn();
    p.selfCare();
    t.is(p.canBoost(),false);
    p.endOfTurn();
    const p2 = new app.Player();
    p2.endOfTurn(false);
    p.heal(p2);
    t.is(p.canBoost(),false);
    t.is(p2.canBoost(),false);
});
test('player canLearnCleverness is false while unlocked.',t=>{
    const p = new app.Player();
    t.is(p.canLearnCleverness(), false);
    p.endOfTurn();
    p.endOfTurn();
    t.is(p.canLearnCleverness(), true);
});
test('player canLearnCleverness is false when player have reached max cleverness.',t=>{
    const p = new app.Player();
    p.cleverness = 12;
    p.endOfTurn();
    p.endOfTurn();
    t.is(p.canLearnCleverness(), false);
});
test('player canLearnCleverness switch back to true when player burnout.',t=>{
    const p = new app.Player();
    p.cleverness = 12;
    p.endOfTurn();
    p.endOfTurn();
    p.burnout()
    t.is(p.canLearnCleverness(), true);
});
test(`player.canLearnHeal() is true if unlockTurn is passed`,t=>{
    const p = new app.Player();
    t.is(p.canLearnHeal(), false);
    p.endOfTurn();
    p.endOfTurn();
    p.endOfTurn();
    t.is(p.canLearnHeal(), true);
});
test(`player.canLearnHeal() is false when heal is learned.`,t=>{
    const p = new app.Player();
    p.endOfTurn();
    p.endOfTurn();
    p.endOfTurn();
    t.is(p.canLearnHeal(), true);
    p.learned.heal = true;
    t.is(p.canLearnHeal(), false);
});
test(`player.canLearnHeal() is true when heal is partially learned only.`,t=>{
    const p = new app.Player();
    p.endOfTurn();
    p.endOfTurn();
    p.endOfTurn();
    t.is(p.canLearnHeal(), true);
    p.learned.heal = 1;
    t.is(p.canLearnHeal(), true);
});
test(`player.canLearnSelfCare() is true if unlockTurn is passed`,t=>{
    const p = new app.Player();
    t.is(p.canLearnSelfCare(), false);
    p.endOfTurn();
    p.endOfTurn();
    p.endOfTurn();
    p.endOfTurn();
    t.is(p.canLearnSelfCare(), true);
});
test(`player.canLearnSelfCare() is false when heal is learned.`,t=>{
    const p = new app.Player();
    p.endOfTurn();
    p.endOfTurn();
    p.endOfTurn();
    p.endOfTurn();
    t.is(p.canLearnSelfCare(), true);
    p.learned.selfCare = true;
    t.is(p.canLearnSelfCare(), false);
});
test('player.canHeal() is true when you have learned heal skill',t=>{
    const p = new app.Player();
    t.is(p.canHeal(), false);
    p.learned.heal = true;
    t.is(p.canHeal(), true);
});
test('player.canSelfCare() is true when you have learned selfCare skill and more tension than minimum',t=>{
    const p = new app.Player();
    t.is(p.canSelfCare(), false);
    p.learned.selfCare = true;
    p.tension = 2;
    t.is(p.canSelfCare(), true);
    p.selfCare();
    p.applyHeal();
    t.is(p.canSelfCare(), false);
});
test(`when a player produce an event is send`,t=>{
    const p = new app.Player();
    t.plan(1);
    const listener = ()=>t.pass();
    p.addFollower(listener);
    p.produce();
});
test(`when a player fail to produce they send a Fail event`,t=>{
    t.plan(1);
    const p = new app.Player();
    mock.random = ()=>0;
    const listener = (ev)=>t.is(ev.name,'Fail');
    p.addFollower(listener);
    p.produce();
});
test(`when a player fail to produce they tension rise`,t=>{
    const p = new app.Player();
    mock.random = ()=>0;
    p.produce();
    p.applyTension();
    t.is(p.tension,2);
});
test(`when a player succeed to produce they send a event with production amount`,t=>{
    t.plan(1);
    const p = new app.Player();
    mock.random = ()=>0.99;
    const listener = (ev)=>t.is(ev.qty,5);
    p.addFollower(listener);
    p.produce();
});
test('player.boost() after production can add some extra production',t=>{
    t.plan(4);
    const p = new app.Player();
    const mockResults = [0.5,0,0.99];
    mock.random = ()=>mockResults.shift();
    const listener = (ev)=>t.true(ev.name==='Fail' || ev.qty>=3);
    p.addFollower(listener);
    p.produce();
    p.boost();
    p.endOfTurn();
    t.is(p.history,'#1[C6T1]P3B+4F');
});
test('player.heal(player2) does nothing if i have more tension than player2',t=>{
    const p = new app.Player();
    p.tension = 5;
    const p2 = new app.Player();
    p2.tension = 3;
    p.heal(p2);
    t.is(p2.pending.heal,0);
});
test(`player.heal(player2) cut down player2 tension to yours if you have less tension`,t=>{
    const p = new app.Player();
    p.tension = 2;
    const p2 = new app.Player();
    p2.tension = 5;
    p.heal(p2);
    t.is(p2.pending.heal,3);
});
test(`when you're healed, your turn action is player.healed()`, t=>{
    const p2 = new app.Player();
    p2.healed();
    t.pass();
});
test('player.learnHeal() need enough success to learn the skill',t=>{
    const p = new app.Player();
    mock.random = ()=>0.99;
    p.learnHeal();
    t.is(p.canHeal(),false)
    p.endOfTurn();
    p.learnHeal();
    t.is(p.canHeal(),true)
});
test('player.learnHeal() can be learned faster with boost',t=>{
    const p = new app.Player();
    mock.random = ()=>0.99;
    p.learnHeal();
    p.boost();
    t.is(p.canHeal(),true);
});
test('player.learnSelfCare() need enough success to learn the skill',t=>{
    const p = new app.Player();
    p.tension = 2;
    mock.random = ()=>0.99;
    p.learnSelfCare();
    t.is(p.canSelfCare(),false)
    p.endOfTurn();
    p.learnSelfCare();
    t.is(p.canSelfCare(),true)
});
test('player.learnSelfCare() can be learned faster with boost',t=>{
    const p = new app.Player();
    p.tension = 2;
    mock.random = ()=>0.99;
    p.learnSelfCare();
    p.boost();
    t.is(p.canSelfCare(),true);
});
test('player.learnSelfCare() can fail if no luck',t=>{
    const p = new app.Player();
    mock.random = ()=>0;
    p.learnSelfCare();
    p.boost();
    t.is(p.canSelfCare(),false);
    t.is(p.pending.tension,3);
});
test('player.learnCleverness() is easier to lvlUp to 8 than to 12',t=>{
    const p = new app.Player();
    mock.random = ()=>0.99;
    t.is(p.cleverness,6);
    p.learnCleverness();
    t.is(p.cleverness,6);
    p.learnCleverness();
    t.is(p.cleverness,8);
    p.learnCleverness();
    t.is(p.cleverness,8);
    p.learnCleverness();
    t.is(p.cleverness,10);
    p.learnCleverness();
    t.is(p.cleverness,10);
    p.learnCleverness();
    t.is(p.cleverness,10);
    p.learnCleverness();
    t.is(p.cleverness,12);
});
test('player.learnCleverness() also work with boost',t=>{
    const p = new app.Player();
    mock.random = ()=>0.99;
    t.is(p.cleverness,6);
    p.learnCleverness();
    p.boost()
    t.is(p.cleverness,8);
    p.learnCleverness();
    t.is(p.cleverness,10);
    p.boost();
    t.is(p.cleverness,10);
    p.learnCleverness();
    t.is(p.cleverness,12);
});
test('player.setThisTurnAction(player.produce) will prepare but not run action.',t=>{
    const p = new app.Player();
    const listener = ()=>t.fail();
    p.addFollower(listener);
    p.setAction('produce');
    t.pass();
});
test(`setThisTurnAction and executePendingAction also work with heal and it's target`,t=>{
    const p = new app.Player();
    const p2 = new app.Player();
    p2.tension = 5;
    p.setAction(['heal',p2]);
    p.executePendingAction();
    p2.endOfTurn();
    t.is(p2.tension,1);
});
test('player.executePendingAction() do the planned task',t=>{
    const p = new app.Player();
    t.plan(1);
    mock.random = ()=>0;
    const listener = (ev)=>t.is(ev.name,'Fail');
    p.addFollower(listener);

    p.setAction('produce');
    p.executePendingAction();
});
test('player.setBoost(false) will not boost on executePendingAction',t=>{
    const p = new app.Player();
    t.plan(1);
    mock.random = ()=>0;
    const listener = (ev)=>t.is(ev.name,'Fail');
    p.addFollower(listener);

    p.setAction('produce');
    p.executePendingAction();
    p.setBoost(false);
    p.executePendingAction();
});
test('player.setBoost() will boost on executePendingAction',t=>{
    const p = new app.Player();
    t.plan(3);
    mock.random = ()=>0;
    const listener = (ev)=>t.is(ev.name,'Fail');
    p.addFollower(listener);

    p.setAction('produce');
    p.executePendingAction();
    p.setBoost(true);
    p.executePendingAction();
});
test('player.resolveSpread() add pending tension if bad luck',t=> {
    const p = new app.Player();
    p.pending.spread = 3;
    const randomMockedResult = [0,1.99/6,1]
    mock.random = ()=>randomMockedResult.pop();
    p.resolveSpread();
    p.applyTension();
    t.is(p.tension,2);
});
test('player.resolveSpread() add pending tension if bad luck depending of existing tension',t=> {
    const p = new app.Player();
    p.turn = 6;
    p.tension = 2;
    p.pending.spread = 3;
    const randomMockedResult = [0,1.99/6,1]
    mock.random = ()=>randomMockedResult.pop();
    p.resolveSpread();
    p.applyTension();
    t.is(p.tension,4);
});
test('player.selfCare() & player.applyHeal() lower tension by 1',t=> {
    const p = new app.Player();
    p.tension = 5;
    p.selfCare();
    t.is(p.tension,5);
    p.applyHeal();
    t.is(p.tension,4);
});
test('player.applyHeal() reset pending heal',t=> {
    const p = new app.Player();
    p.tension = 5;
    p.selfCare();
    p.applyHeal();
    t.is(p.tension,4);
    p.selfCare();
    p.applyHeal();
    t.is(p.tension,3);
});
test(`player.applyHeal() can't put tension under 1`,t=> {
    const p = new app.Player();
    t.is(p.tension,1);
    p.selfCare();
    p.applyHeal();
    t.is(p.tension,1);
});
test(`player.applyTension() add pending tension to player`,t=> {
    const p = new app.Player();
    t.is(p.tension,1);
    p.turn = 6;
    p.pending.tension = 2;
    p.applyTension();
    t.is(p.tension,3);
});
test(`player.applyTension() limit tension gain during easyStart period`,t=> {
    const p = new app.Player();
    t.is(p.tension,1);
    p.pending.tension = 2
    p.applyTension();
    t.is(p.tension,2);
    p.endOfTurn();
    p.endOfTurn();
    p.endOfTurn();
    p.endOfTurn();
    p.endOfTurn();
    p.pending.tension = 2
    p.applyTension();
    t.is(p.tension,4);
});

test(`player.applyTension() trigger burnout() when needed`,t=> {
    const p = new app.Player();
    p.tension = 5;
    p.pending.tension = 2
    p.applyTension();
    t.is(p.cleverness,4);
});
test(`player.endOfTurn(false) if the team haven't produce enough will add extra tension`,t=>{
    const p = new app.Player();
    p.endOfTurn(false);
    t.is(p.tension,2);
});
test(`player.endOfTurn() reset immunities`,t=>{
    const p = new app.Player();
    p.immune.tension=true
    p.endOfTurn();
    t.is(p.immune.tension,0);
});

test(`when a player burnout they can't burnout again before recovery`,t=> {
    const p = new app.Player();
    p.cleverness = 8;
    p.tension = 7;
    p.pending.tension = 2
    p.applyTension();
    t.is(p.cleverness,6);
    p.applyTension();
    t.is(p.cleverness,6);
});
test(`after recovery, a player can burnout again`,t=> {
    const p = new app.Player();
    p.turn = 6;
    p.cleverness = 8;
    p.tension = 7;
    p.pending.tension = 2
    p.applyTension();
    t.is(p.cleverness,6);
    p.pending.heal = 4
    p.applyHeal();
    p.pending.tension = 2
    p.applyTension();
    t.is(p.cleverness,4);
});
test(`when a player recover from a burnout, it send a Recovery event`,t=> {
    t.plan(1);
    const p = new app.Player();
    p.cleverness = 8;
    p.tension = 7;
    p.pending.tension = 2
    p.applyTension();
    const listener = (ev)=>t.is(ev.name,'Recovery');
    p.addFollower(listener);

    p.pending.heal = 4
    p.applyHeal();
});
test(`burnout can't send you under minimum cleverness`,t=> {
    const p = new app.Player();
    p.cleverness = 4;
    p.tension = 3;
    p.pending.tension = 2
    p.applyTension();
    t.is(p.cleverness,4);
});
test(`player.selfCare() set player immune to new tension and spread for the turn player.applyTension() does nothing when immune`,t=> {
    const p = new app.Player();
    t.is(p.tension,1);
    p.pending.tension = 2
    p.selfCare();
    p.applyTension();
    t.is(p.tension,1);
});
test(`in gameVariant(3) player.healed() set player immune to new tension and spread for the turn player.applyTension() does nothing when immune`,t=> {
    const p = new app.Player(getRules(3));
    t.is(p.tension,1);
    p.pending.tension = 2
    p.healed();
    p.applyTension();
    t.is(p.tension,1);
});

test('when a player.burnout() it loose some cleverness',t=>{
    const p = new app.Player();
    t.is(p.cleverness,6);
    p.burnout();
    t.is(p.cleverness,4);
});
test('when a player.burnout() it learn selfCare ability',t=>{
    const p = new app.Player();
    t.falsy(p.learned.selfCare);
    p.burnout();
    t.true(p.learned.selfCare);
});
test('when a player.burnout() it say it to subscribers',t=>{
    const p = new app.Player();
    t.plan(1);
    const listener = (ev)=>t.is(ev.name,'Burnout');
    p.addFollower(listener);
    p.burnout();
});

test('player.status()',t=>{
    const p = new app.Player();
    t.is(p.status(),'[C6T1]');
    p.tension = 2;
    t.is(p.status(),'[C6T2]');
    p.cleverness = 12;
    t.is(p.status(),'[C12T2]');
    p.learned.heal = true;
    t.is(p.status(),'[C12T2H]');
    p.learned.selfCare = true;
    t.is(p.status(),'[C12T2HSC]');
    p.learned.heal = false;
    t.is(p.status(),'[C12T2SC]');
});
test('player.history() return status and action of this player since the beginning',t=>{
    const p = new app.Player();
    t.is(p.turnHistory,'#1[C6T1]');
    mock.random = ()=>0;
    p.produce()
    t.is(p.turnHistory,'#1[C6T1]PF');
    mock.random = ()=>0.99;
    p.boost()
    t.is(p.turnHistory,'#1[C6T1]PFB7');
    p.endOfTurn()
    t.is(p.history,'#1[C6T1]PFB7');
    t.is(p.turnHistory,'#2[C6T2]');
    p.selfCare()
    t.is(p.turnHistory,'#2[C6T2]SC');
    p.endOfTurn()
    t.is(p.history,'#1[C6T1]PFB7#2[C6T2]SC');
    p.endOfTurn()
    t.is(p.history,'#1[C6T1]PFB7#2[C6T2]SC#3[C6T1]');
});
