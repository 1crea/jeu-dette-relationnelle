import {Team} from './Team.mjs';
import {PubSub} from "./PubSub.mjs";
export class GameSimulator extends PubSub{
    constructor(size,ai,rules) {
        super()
        this.team = new Team(size,ai,rules);
        this.team.subscribe(this);
        this.stats = {};
    }
    whenWin(ev){
        this.stats.winTurn = ev.sender.turn;
    }
    whenDefeat(ev){
        this.stats.defeatTurn = ev.sender.turn;
        this.stats.defeatCause = ev.cause;
    }
    play(statTurnList){
        const maxTurn = statTurnList.reduce((acc,cur)=>Math.max(acc,cur));
        for(let i=1;i<=maxTurn;i++) if(!this.stats.defeatTurn) this.team.playFullTurn();
        statTurnList.forEach(turn=>{
           if(!this.stats.defeatTurn) this.stats[`turn${turn}`] = {survive:true};
           else this.stats[`turn${turn}`] = {survive:this.stats.defeatTurn>turn};
        });
        return this.stats;
    }
}
export class BatchGameSimulator{
    constructor(batchSize, teamSize, ai, rules) {
        this.batchSize = batchSize;
        this.teamSize = teamSize;
        this.ai = ai;
        this.rules = rules;
    }
    generateStats(statTurnList){
        const stats = {};
        const rounds = [];
        for(let i = 0;i<this.batchSize;i++){
            const gs = new GameSimulator(this.teamSize,this.ai,this.rules);
            rounds.push(gs.play(statTurnList));
        }
        stats.winRate = rounds.filter(r=>r.winTurn).length/this.batchSize;
        const defeatCount = rounds.filter(r=>r.defeatTurn).length
        stats.defeatRate = defeatCount/this.batchSize;
        stats.defeatRateByBankrupt = rounds.filter(r=>r.defeatCause==='bankrupt').length/defeatCount;
        stats.defeatRateBySimultaneousBurnout = rounds.filter(r=>r.defeatCause==='inBurnout').length/defeatCount;
        stats.defeatRateByOverallBurnout = rounds.filter(r=>r.defeatCause==='hadBurnout').length/defeatCount;
        statTurnList.forEach(turn=>{
            stats[`turn${turn}`] = {surviveRate:rounds.filter(r=>r[`turn${turn}`].survive).length/this.batchSize};
        });
        return stats;
    }
}