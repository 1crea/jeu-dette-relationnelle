/*
import {Runner} from './Runner.mjs'

const run = new Runner(1000);
run.run(5);
console.log(`\n-------------------\nTour 5 terminé\n-------------------\n`);
run.run(5);
console.log(`\n-------------------\nTour 10 terminé\n-------------------\n`);
run.run(5);
console.log(`\n-------------------\nTour 15 terminé\n-------------------\n`);
run.run(85);
console.log(`\n-------------------\nTour 100 terminé\n-------------------\n`);

 */
import {BatchGameSimulator, GameSimulator} from "./GameSimulator.mjs";
import {cleverCare, fullCare, halfSupport, prodAndBoost, quartSupport, tierSupport} from "./ai.mjs";
import {getRules} from "./gameVariants.mjs";


//console.log(getRules(4).boost.autoUnlockTurn);
//const gs = new GameSimulator(5, fullCare,getRules(4));
//const stat = gs.play([100]);
//console.log(`@${5} : `, gs.team.history, stat);
//gs.team.players.forEach(p=>console.log(p.history));
for(let s=1;s<=10;s++) console.log(`@${s} : `, (new BatchGameSimulator(1000,s, fullCare,getRules(4))).generateStats([1,5,10,15,100]));
//for(let s=20;s<=50;s+=10) console.log(`@${s} : `, (new BatchGameSimulator(1000,s, quartSupport)).generateStats([1,5,10,15,100]));
