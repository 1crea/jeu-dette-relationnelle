import test from 'ava';
import * as app from './utils.mjs';

/*** rollDice(cleverness, tension) ***/

test('rollDice always negative if tension > cleverness', t=>{
    const res =  app.rollDice(6,7);
    t.true(res<0)
})
test('rollDice always >=0 if tension<=1', t=>{
    const res =  app.rollDice(6,1);
    t.true(res>=0)
})

test('rollDice(6,1) with random:1 will return 5', t=>{
    app.staticFunc.random = ()=>0.999
    t.is(app.rollDice(6,1), 5)
})
test('rollDice(8,1) with random:1 will return 7', t=>{
    app.staticFunc.random = ()=>0.999
    t.is(app.rollDice(8,1), 7)
})
test('rollDice(8,1) with random:0 will return 0', t=>{
    app.staticFunc.random = ()=>0.001
    t.is(app.rollDice(8,1), 0)
})
test('rollDice(8,2) with random:0 will return -1', t=>{
    app.staticFunc.random = ()=>0.001
    t.is(app.rollDice(8,2), -1)
})

