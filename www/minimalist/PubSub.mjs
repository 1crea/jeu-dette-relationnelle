export class PubSub{
    constructor() {
        this._subscribers = [];
    }
    publish(ev){
        this._subscribers.forEach(follower=>{
            if(typeof follower === 'function') follower(ev);
            else follower.eventRouter(ev);
        });
    }
    subscribe(callback){
        this.addFollower(callback);
    }
    addFollower(callback){
        this._subscribers.push(callback);
    }
    eventRouter(ev){
        this[`when${ev.name}`](ev);
    }
}
export function addPubSub(obj) {
    const tmp = new PubSub();
    obj._subscribers = [];
    obj.publish = tmp.publish;
    obj.addFollower = tmp.addFollower;
    obj.eventRouter = tmp.eventRouter;
    return obj;
}

export function methodWrapper(obj, listenerMethodName){
    return (ev)=>obj[listenerMethodName](ev);
}