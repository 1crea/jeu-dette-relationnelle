import test from 'ava';
import * as app from './Team.mjs';
import {staticFunc as mock} from "./utils.mjs";
import {getAI} from "./gameVariants.mjs";

function testHelper_fastForwardTeamTurns(team, turn){
    for(let t=0;t<turn;t++){
        team.pending.production=0;
        team.endOfTurn();
    }
}
test(`can create a team with 1 player`,t=>t.truthy(new app.Team(1)));
test(`can create a team with 10 players`,t=>{
    const team = new app.Team(10);
    t.is(team.players.length,10);
})
test(`can create a team with 100 players`,t=>{
    const team = new app.Team(100);
    t.is(team.players[99].cleverness,6);
})
test(`team count player in burnout`,t=>{
    const team = new app.Team(10);
    testHelper_fastForwardTeamTurns(team, 6);
    for(let i=0;i<6;i++) {
        team.players[i].pending.tension = 6;
        team.players[i].applyTension();
    }
    t.is(team.inBurnout,6);
})
test(`team count player burnouts`,t=>{
    const team = new app.Team(10);
    testHelper_fastForwardTeamTurns(team, 6);
    for(let i=0;i<6;i++) {
        team.players[i].pending.tension = 6;
        team.players[i].applyTension();
    }
    for(let i=6;i<10;i++) {
        team.players[i].heal(team.players[i-6]);
    }
    team.endOfTurn();
    t.is(team.burnouts,6);
    t.is(team.inBurnout,2);
})
test(`team broadcast player fail as spread to other players`,t=>{
    const team = new app.Team(10);
    team.players[0].fail();
    t.is(team.players[0].pending.tension,1);
    t.is(team.players[0].pending.spread,0);
    t.is(team.players[1].pending.spread,1);
    t.is(team.players[9].pending.spread,1);
    team.players[1].fail();
    t.is(team.players[0].pending.spread,1);
    t.is(team.players[1].pending.spread,1);
    t.is(team.players[9].pending.spread,2);
});
test(`team use external AI to assign action to they members`,t=>{
    const ai = {};
    ai.chooseTeamActions = (team)=>{
        return team.players.map(p=>'onlyForTest');
    }
    const team = new app.Team(10,ai);
    t.is(team.players[0].pending.actions.length,0);
    team.chooseActions();
    t.is(team.players[0].pending.actions[0],'onlyForTest');
});
test(`team use external AI to assign boost to they members`,t=>{
    const ai = {};
    ai.chooseTeamActions = ()=>{
        return ['produce','produce','produce'];
    }
    ai.chooseTeamBoosts = ()=>{
        return [true,true,true];
    }
    const team = new app.Team(3,ai);
    t.is(team.players[0].pending.actions.length,0);
    team.chooseActions();
    team.executeActions();
    team.chooseBoosts();
    t.is(team.players[0].pending.actions[0],'boost');
    team.executeBoosts();
    t.is(team.players[0].pending.actions.length,0);
});
test(`team append each player production minus consumption/salary`,t=>{
    const team = new app.Team(10);
    mock.random = ()=>0.99;
    team.chooseActions();
    team.executeActions();
    t.is(team.pending.production,40);
});
test(`if team members product enough, end of turn don't add tension`,t=>{
    const team = new app.Team(10);
    mock.random = ()=>0.99;
    team.chooseActions();
    team.executeActions();
    team.endOfTurn();
    team.players.forEach(p=>t.is(p.tension,1));
});
test(`if team members dont product enough, end of turn add tension`,t=>{
    const team = new app.Team(10);
    team.endOfTurn();
    team.players.forEach(p=>t.is(p.tension,2));
});
test(`team check if win conditions are met`,t=>{
    t.plan(3);
    const team = new app.Team(10);
    team.subscribe((ev)=>t.is(ev.name,'Win'));

    testHelper_fastForwardTeamTurns(team, 10);
    team.pending.production=110;
    t.falsy(team.haveWin);
    team.endOfTurn();
    t.true(team.haveWin);
});
test(`team have to survive up to turn 10 to be able to win`,t=>{
    t.plan(3);
    const team = new app.Team(10);
    team.subscribe((ev)=>t.is(ev.name,'Win'));
    team.pending.production=110;
    team.endOfTurn();
    testHelper_fastForwardTeamTurns(team, 8);
    t.falsy(team.haveWin);
    testHelper_fastForwardTeamTurns(team, 1);
    t.true(team.haveWin);
});
test(`if team have win, it cant win again in the same game`,t=>{
    t.plan(1);
    const team = new app.Team(10);
    team.subscribe((ev)=>t.is(ev.name,'Win'));
    team.pending.production=110;
    team.endOfTurn();
    testHelper_fastForwardTeamTurns(team, 20);
});
test(`team check if defeat conditions are met (half the team or more in burnout)`,t=>{
    t.plan(2);
    const team = new app.Team(10);
    team.subscribe((ev)=>{
        t.is(ev.name,'Defeat')
        t.is(ev.cause,'inBurnout')
    });
    team.pending.production=0;
    team.inBurnout=5
    team.endOfTurn();
});
test(`team check if defeat conditions are met (1 time the size of the team had burnout)`,t=>{
    t.plan(2);
    const team = new app.Team(10);
    team.subscribe((ev)=>{
        t.is(ev.name,'Defeat')
        t.is(ev.cause,'hadBurnout')
    });
    team.pending.production=0;
    team.burnouts=10
    team.endOfTurn();
});
test(`team check if defeat conditions are met (productionStock < 0)`,t=>{
    t.plan(2);
    const team = new app.Team(10);
    team.subscribe((ev)=>{
        t.is(ev.name,'Defeat')
        t.is(ev.cause,'bankrupt')
    });
    team.productionStock=0;
    team.pending.production=-1;
    team.endOfTurn();
});
test(`team.playFullTurn() run a full turn for all it members and self`,t=>{
    mock.random = ()=>0.99;
    const team = new app.Team(10,getAI(2));
    team.playFullTurn();
    team.playFullTurn();
    t.is(team.history,'<({ GameVariant:defaultRules ~ AI:prodAndBoost ~ TeamSize:10 })> #1[P40S0B0C0]#2[P60S40B0C0]');
    team.players.forEach(p=>t.is(p.history,'#1[C6T1]P5#2[C6T1]P5B+2'));
});

test.todo(`team call every turn step to go to next turn`)
