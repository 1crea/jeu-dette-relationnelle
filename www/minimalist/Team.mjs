import {Player} from "./Player.mjs";
import {PubSub} from "./PubSub.mjs";
import {activeAI, activeRules} from "./gameVariants.mjs";
import {Defeat, Win} from "./events.mjs";

export class Team extends PubSub{
    constructor(size,ai=undefined,rules=undefined) {
        super()
        this.players = [];
        for(let i = 0 ; i<size ; i++ ) this.players.push(new Player(rules));
        this.players.forEach(p=>p.addFollower(this));
        this.pending = {production:-size};
        this.productionStock = 0;
        this.burnouts = 0;
        this.inBurnout = 0;
        this.gameVariant = rules || activeRules;
        this.ai = ai || activeAI;
        this.turn = 1;
        this.history = `<({ GameVariant:${this.gameVariant.name} ~ AI:${this.ai.name} ~ TeamSize:${size} })> `;
    }
    playFullTurn(){
        //this.pickAnEvent()?
        this.chooseActions()
        this.executeActions()
        this.chooseBoosts()
        this.executeBoosts()
        this.endOfTurn()
    }
    chooseActions(){
        const actionList = this.ai.chooseTeamActions(this);
        this.players.forEach((p,i)=>p.setAction(actionList[i]));
    }
    executeActions(){
        this.players.forEach(p=>p.executePendingAction());
    }
    chooseBoosts(){
        const actionList = this.ai.chooseTeamBoosts(this);
        this.players.forEach((p,i)=>p.setBoost(actionList[i]));
    }
    executeBoosts(){
        this.executeActions();
    }
    endOfTurn(){
        this.players.forEach(p=>p.endOfTurn(this.pending.production>=0));
        this.history += `#${this.turn}${this.status()}`;
        this.productionStock += this.pending.production;
        this.pending.production=-this.players.length;
        this.isDefeat();
        this.isWin();
        this.turn++;
    }
    isDefeat(){
        function active(condition){
            return condition !== -1 && condition !== false && condition !== undefined ;
        }
        if(active(this.gameVariant.defeatConditions.burnout.maxSimultaneous) && this.inBurnout >= this.players.length * this.gameVariant.defeatConditions.burnout.maxSimultaneous){
            this.publish(new Defeat(this,'inBurnout'));
            return true;
        }
        if(active(this.gameVariant.defeatConditions.burnout.overallMax) && this.burnouts >= this.players.length * this.gameVariant.defeatConditions.burnout.overallMax){
            this.publish(new Defeat(this,'hadBurnout'));
            return true;
        }
        if(active(this.gameVariant.defeatConditions.bankrupt.underStock) && this.productionStock<this.gameVariant.defeatConditions.bankrupt.underStock){
            this.publish(new Defeat(this,'bankrupt'));
            return true;
        }
        return false;
    }
    isWin(){
        if(this.haveWin) return false;
        if(this.turn<10) return false;
        if(this.productionStock-this.debts()<100) return false;
        this.haveWin = true;
        this.publish(new Win(this));
        return true;
    }
    debts(){
        return this.players.reduce((acc,p)=>acc+p.tension,0);
    }
    status(){
        return `[P${this.pending.production}S${this.productionStock}B${this.inBurnout}C${this.burnouts}]` // PendingProd ProdStock inBurnout CumulatedBurnout
    }
    whenBurnout(){
        this.burnouts++;
        this.inBurnout++;
    }
    whenRecovery(){
        this.inBurnout--;
    }
    whenFail(ev){
        this.players.forEach(p=>{
            if(p !== ev.sender)
            p.pending.spread++;
        });
    }
    whenProd(ev){
        this.pending.production += ev.qty
    }
}