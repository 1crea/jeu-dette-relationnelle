import test from 'ava';
import * as app from './events.mjs';

test('i can create new Fail(player) event',t => t.truthy(new app.Fail({})));
test('i can create new Burnout(player) event',t => t.truthy(new app.Burnout({})));
test('i can create new Recovery(player) event',t => t.truthy(new app.Recovery({})));
test('any events expose it sender.',t=> {
    const player = {};
    const ev = new app.Fail(player);
    t.is(ev.sender,player);
    t.is((new app.Burnout(player)).sender,player);
});

test('any events have there type as name.',t=> {
    const ev = new app.Fail({});
    t.is(ev.name,'Fail');
});
