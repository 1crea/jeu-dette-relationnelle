export const opnb =         {
    name:'OPNB', // only prod never boost
    chooseTeamActions: (team)=>team.players.map(p=>'produce'),
    chooseTeamBoosts: (team)=>team.players.map(p=>false)
};
export const prodAndBoost = {
    name:'prodAndBoost',
    chooseTeamActions: (team)=>team.players.map(p=>'produce'),
    chooseTeamBoosts: (team)=>team.players.map(p=>p.canBoost())
};
export const halfSupport = {
    name:'halfSupport',
    chooseTeamActions: (team)=>{
        const actions = [];
        if(team.productionStock<team.players.length) return team.players.map(p=>'produce');

        for(let even = 0; even<team.players.length;even+=2){
            if(team.players[even].canSelfCare()) actions[even]='selfCare';
            else if(team.players[even].canLearnCleverness()) actions[even]='learnCleverness';
            else actions[even]='produce';
        }
        for(let odd = 1; odd<team.players.length;odd+=2){
            if(team.players[odd].canSelfCare()) actions[odd]='selfCare';
            else if(team.players[odd].canHeal() && team.players[odd-1].tension-2>team.players[odd].tension) {
                actions[odd]=['heal',team.players[odd-1]];
                actions[odd-1]='healed';
            }
            else if(team.players[odd].canLearnSelfCare()) actions[odd]='learnSelfCare';
            else if(team.players[odd].canLearnHeal()) actions[odd]='learnHeal';
            else actions[odd]='produce';
        }
        return actions;
    },
    chooseTeamBoosts: (team)=>team.players.map(p=>(p.tension === 1 || (team.productionStock<team.players.length && p.tension<p.cleverness/2)) && p.canBoost()),
};
export const quartSupport = {
    name:'1/4Support',
    chooseTeamActions: (team)=>{
        const actions = [];
        if(team.productionStock<team.players.length) return team.players.map(p=>'produce');
        team.players.forEach((p,i)=>{
            if(i%4){
                if(actions[i]!=='healed'){
                    if(p.canSelfCare() && p.tension>p.cleverness/2) actions[i]='selfCare';
                    else if(p.canLearnCleverness()) actions[i]='learnCleverness';
                    else actions[i]='produce';
                }
            } else {
                if(p.canSelfCare()) actions[i]='selfCare';
                else if(p.canHeal() && team.players.filter((p,index)=>p.tension>3 && actions[index]!=='healed').length) {
                    const bestTarget = team.players.filter((p,index)=>p.tension>3 && actions[index]!=='healed').sort((a,b)=>b.tension-a.tension).shift();
                    actions[i]=['heal',bestTarget];
                    let targetIndex;
                    team.players.forEach((p,index)=>{if(p===bestTarget) targetIndex = index});
                    actions[targetIndex]='healed';
                }
                else if(p.canLearnSelfCare()) actions[i]='learnSelfCare';
                else if(p.canLearnHeal()) actions[i]='learnHeal';
                else actions[i]='produce';
            }
        })
        return actions;
    },
    chooseTeamBoosts: (team)=>team.players.map(p=>(p.tension === 1 || (team.productionStock<team.players.length && p.tension<p.cleverness/2)) && p.canBoost()),
};
export const tierSupport = {
    name:'tierSupport',
    chooseTeamActions: (team)=>{
        const actions = [];
        if(team.productionStock<team.players.length) return team.players.map(p=>'produce');
        team.players.forEach((p,i)=>{
            if(i%3){
                if(actions[i]!=='healed'){
                    if(p.canSelfCare() && p.tension>p.cleverness/2) actions[i]='selfCare';
                    else if(p.canLearnCleverness()) actions[i]='learnCleverness';
                    else actions[i]='produce';
                }
            } else {
                if(p.canSelfCare()) actions[i]='selfCare';
                else if(p.canHeal() && team.players.filter((p,index)=>p.tension>3 && actions[index]!=='healed').length) {
                    const bestTarget = team.players.filter((p,index)=>p.tension>3 && actions[index]!=='healed').sort((a,b)=>b.tension-a.tension).shift();
                    actions[i]=['heal',bestTarget];
                    let targetIndex;
                    team.players.forEach((p,index)=>{if(p===bestTarget) targetIndex = index});
                    actions[targetIndex]='healed';
                }
                else if(p.canLearnSelfCare()) actions[i]='learnSelfCare';
                else if(p.canLearnHeal()) actions[i]='learnHeal';
                else actions[i]='produce';
            }
        })
        return actions;
    },
    chooseTeamBoosts: (team)=>team.players.map(p=>(p.tension === 1 || (team.productionStock<team.players.length && p.tension<p.cleverness/2)) && p.canBoost()),
};
export const fullCare = {
    name:'fullCare',
    chooseTeamActions: (team)=>{
        const actions = [];
        const bestTargets = team.players.filter((p,index)=>p.tension>3).sort((a,b)=>b.tension-a.tension);
        team.players.forEach((p,i)=>{
            if(p.canSelfCare()) actions[i]='selfCare';
            else if(p.canLearnSelfCare()) actions[i]='learnSelfCare';
            else if(p.canHeal() && bestTargets.length) {
                const bestTarget = bestTargets.shift();
                actions[i]=['heal',bestTarget];
                let targetIndex;
                team.players.forEach((p,index)=>{if(p===bestTarget) targetIndex = index});
                actions[targetIndex]='healed';
            }
            else if(p.canLearnHeal()) actions[i]='learnHeal';
            else if(p.canLearnCleverness()) actions[i]='learnCleverness';
            else actions[i]='produce';
        });
        if(team.productionStock<team.players.length && team.players.filter((p,i)=>actions[i]==='produce').reduce((acc,cur)=>{return acc+(cur.cleverness-cur.tension)*(cur.cleverness-cur.tension)/cur.cleverness/3 /* /2 = avg */},0)<team.players.length) return team.players.map(p=>'produce');
        /*team.players.forEach((p,i)=>{
            if(team.players.filter((p,i)=>actions[i]==='produce').reduce((acc,cur)=>{return acc+(cur.cleverness-cur.tension)*(cur.cleverness-cur.tension)/cur.cleverness/2 },0)<team.players.length){
                team.players.forEach((p,i)=>{
                    if(typeof actions[i] === 'string' && actions[i].split('learn').length===2 && Math.random()>0.5) actions[i] = 'produce';
                });
            }
        });*/
        return actions;
    },
    chooseTeamBoosts: (team)=>team.players.map(p=>(p.tension === 1 || (team.productionStock<team.players.length && team.pending.production<team.players.length && p.log[p.log.length-1] === 'produce' && p.tension<p.cleverness/2)) && p.canBoost()),
};
export const cleverCare = {
    name:'cleverCare',
    chooseTeamActions: (team)=>{
        const actions = [];
        const bestTargets = team.players.filter((p,index)=>p.tension>3).sort((a,b)=>b.tension-a.tension);
        team.players.forEach((p,i)=>{
            if(p.canSelfCare()) actions[i]='selfCare';
            else if(p.canLearnSelfCare()) actions[i]='learnSelfCare';
            else if(p.canLearnCleverness()) actions[i]='learnCleverness';
            else if(p.canHeal() && bestTargets.length) {
                const bestTarget = bestTargets.shift();
                actions[i]=['heal',bestTarget];
                let targetIndex;
                team.players.forEach((p,index)=>{if(p===bestTarget) targetIndex = index});
                actions[targetIndex]='healed';
            }
            else if(p.canLearnHeal()) actions[i]='learnHeal';
            else actions[i]='produce';
        });
        if(team.productionStock<team.players.length && team.players.filter((p,i)=>actions[i]==='produce').reduce((acc,cur)=>{return acc+(cur.cleverness-cur.tension)*(cur.cleverness-cur.tension)/cur.cleverness/3 /* /2 = avg */},0)<team.players.length) return team.players.map(p=>'produce');
        team.players.forEach((p,i)=>{
            if(team.players.filter((p,i)=>actions[i]==='produce').reduce((acc,cur)=>{return acc+(cur.cleverness-cur.tension)*(cur.cleverness-cur.tension)/cur.cleverness/2 },0)<team.players.length){
                team.players.forEach((p,i)=>{
                    if(typeof actions[i] === 'string' && actions[i].split('learn').length===2 && Math.random()>0.5) actions[i] = 'produce';
                });
            }
        });
        return actions;
    },
    chooseTeamBoosts: (team)=>team.players.map(p=>(p.tension === 1 || (team.productionStock<team.players.length && team.pending.production<team.players.length && p.log[p.log.length-1] === 'produce' && p.tension<p.cleverness/2)) && p.canBoost()),
};
