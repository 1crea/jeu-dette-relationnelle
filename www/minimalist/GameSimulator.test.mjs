import test from 'ava';
import * as app from './GameSimulator.mjs';
import {staticFunc as mock} from "./utils.mjs";
import {BatchGameSimulator} from "./GameSimulator.mjs";
import {opnb} from "./ai.mjs";
import {activeRules} from "./gameVariants.mjs";

/**
 * Jouer une partie entière avec des règles communes (et des IA qui peuvent varier selon les équipes)
 * Fournir les stat/résultats à différent moment de la partie :
 * - survie à l'issue du premier tour / taux de défait au 1er tour
 * - survie à l'issue du tour 5
 * - survie à l'issue du tour 10
 * - survie à l'issue du tour 15
 * - survie à l'issue du tour 100
 * - tour de victoire (si victoire avant le tour 100)
 * - tour de défaite (si défaite avant le tour 100)
 * Pour toutes ces situations :
 * - le niveau de cleverness min, avg, med, max + quartil + son augmentation moyenne par tour par personne
 * - idem pour le niveau de tension
 * - idem pour les burnout
 * - idem pour la prod
 * - idem pour le nombre de jet de dé (par personne et par tour)
 * - le nombre d'utilisation de chaque compétence (et ramené à par personne et par tour)
 * - le taux d'aquisition de la comp selfCare
 * - le taux d'aquisition de la comp heal
 * - le taux d'aquisition de cleverness 8
 * - le taux d'aquisition de cleverness 10
 * - le taux d'aquisition de cleverness 12
 * - le taux de burnout
 * - le tour de premier burnout
 * - le tour de premier selfCare
 * - le tour de premier heal
 * - fréquence de heal par tour par personne
 * - fréquence de selfCare par tour par personne
 * - fréquence de prod par tour par personne
 * - fréquence de boost(prod) par tour par personne
 * - fréquence de boost(learn) par tour par personne
 * - fréquence de learn* par tour par personne
 * - montant de heal(heal ou selfCare) par tour par personne
 * - montant de heal(heal) par tour par personne
 * - montant de heal(selfCare) par tour par personne
 * - montant de heal(heal) par action de heal (min, max, moyen, median, quartils)
 * - montant de prod par action de prod (min, max, moyen, median, quartils)
 * - montant de boost de prod par action de boost(prod) (min, max, moyen, median, quartils)
 */

test('dummy',t => t.deepEqual(1,1));
test('(new GameSimulator(...)).play() return if the team survive to listed turns and if there is a defeatTurn and a winTurn | HAPPY version',t => {
    mock.random = ()=>0.99;
    const gs = new app.GameSimulator(5);
    const stats = gs.play([1,5,10,15,100]);
    t.is(stats.winTurn,10);
    t.falsy(stats.defeatTurn);
    t.is(stats.turn1.survive,true);
    t.is(stats.turn5.survive,true);
    t.is(stats.turn10.survive,true);
    t.is(stats.turn15.survive,true);
    t.is(stats.turn100.survive,true);
});
test('(new GameSimulator(...)).play() return if the team survive to listed turns and if there is a defeatTurn and a winTurn | SAD version',t => {
    mock.random = ()=>0;
    const stats = (new app.GameSimulator(5)).play([1,5,10,15,100]);
    t.falsy(stats.winTurn);
    t.is(stats.defeatTurn,1);
    t.is(stats.defeatCause,'bankrupt');
    t.is(stats.turn1.survive,false);
    t.is(stats.turn5.survive,false);
    t.is(stats.turn10.survive,false);
    t.is(stats.turn15.survive,false);
    t.is(stats.turn100.survive,false);
});
test('(new BatchGameSimulator(batchSize, teamSize, ai, gameVariant)).generateStats(statTurnList) return win and defeat rate + survive rate à designated turn',t => {
    const randPool = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,0.99];
    mock.random = ()=>{const res = randPool.pop(); randPool.unshift(res); return res;};
    const stats = (new app.BatchGameSimulator(10,2,opnb,activeRules)).generateStats([1,5,10,15,100]);
    t.is(stats.winRate,0);
    t.is(stats.defeatRate,1);
    t.is(stats.defeatRateByBankrupt,0);
    t.is(stats.defeatRateBySimultaneousBurnout,1);
    t.is(stats.defeatRateByOverallBurnout,0);
    t.is(stats.turn1.surviveRate,1);
    t.is(stats.turn5.surviveRate,1);
    t.is(stats.turn10.surviveRate,0);
    t.is(stats.turn15.surviveRate,0);
    t.is(stats.turn100.surviveRate,0);
});
