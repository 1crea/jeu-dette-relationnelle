import {halfSupport, opnb, prodAndBoost, quartSupport} from "./ai.mjs";

const gameVariants = {
    defaults: {
        winConditions:{
            productionStock:100,
            substrateDebtsToStock:true,
            survive:10
        },
        defeatConditions:{
            burnout:{maxSimultaneous:0.5,overallMax:1}, // in team size proportion
            bankrupt:{underStock:0}
        },
        cleverness: {
            min: 4,
            max: 12,
            init: 6,
            learn:{
                unlockTurn:3,
                fromLvl4:{
                    dices:[0],
                    successCount:2,
                    minimalDiceScore:1,
                },
                fromLvl6:{
                    dices:[0],
                    successCount:2,
                    minimalDiceScore:2,
                },
                fromLvl8:{
                    dices:[0],
                    successCount:2,
                    minimalDiceScore:4,
                },
                fromLvl10:{
                    dices:[0],
                    successCount:3,
                    minimalDiceScore:6,
                },
            }
        },
        tension: {
            min: 1,
            init: 1
        },
        easyStart:{
            endOfMercy:5,
            maxPending:{tension:1},
            max:{addict:1},
            immune:{tension:false,dodgeDice:false},
            clevernessBuff:0,
        },
        boost:{
            autoUnlockTurn:2,
            dices:[-2,+2],
            cost:{
                initial: {tension:0,dodgeDice:0,prod:0,addict:1},
            },
        },
        fail:{
            cost:{tension: 1}
        },
        insufficientTeamProd:{
            cost:{tension: 1}
        },
        burnout:{
            cost:{cleverness: 2}
        },
        heal:{
            autoUnlockTurn:-1, // never
            immune:{tension:false,dodgeDice:false},
            healFunction:()=>{},
            learn:{
                unlockTurn:4,
                dices:[0],
                successCount:2,
                minimalDiceScore:1,
            },
        },
        healed:{
            forceUse:true,
            immune:{tension:false,dodgeDice:false},
        },
        selfCare:{
            autoUnlockTurn:-1, // never
            heal:{tension:1,addict:1},
            immune:{tension:true,dodgeDice:true},
            learn:{
                unlockTurn:5,
                dices:[0],
                successCount:2,
                minimalDiceScore:1,
            },
        },
    },
    alt: [
        {name:'defaultRules'},
        {
            name:'alt-1',
            cleverness: {
                init: 8
            },
        },
        {
            name:'alt-2',
            cleverness: {
                init: 12
            },
            canHeal:{
                init:true
            },
            canSelfCare:{
                init:true
            },
        },
        {
            name:'minimalist-progressive',
            cleverness: {
                learn:{
                    unlockTurn:5,
                    fromLvl4:{
                        successCount:1,
                    },
                    fromLvl6:{
                        successCount:2,
                        minimalDiceScore:1,
                    },
                    fromLvl8:{
                        successCount:3,
                        minimalDiceScore:1,
                    },
                    fromLvl10:{
                        successCount:4,
                        minimalDiceScore:1,
                    },
                }
            },
            heal:{
                immune:{tension:false},
                learn:{
                    unlockTurn:3,
                    successCount:2,
                },
            },
            healed:{
                immune:{tension:true},
            },
            selfCare:{
                learn:{
                    unlockTurn:4,
                    successCount:2,
                },
            }
        },
        {
            name:'minimalist-raw-fast',
            easyStart:{
                endOfMercy:15,
            },
            boost:{
                autoUnlockTurn:1,
            },
            cleverness: {
                learn:{
                    unlockTurn:1,
                    fromLvl4:{
                        successCount:1,
                    },
                    fromLvl6:{
                        successCount:1,
                        minimalDiceScore:1,
                    },
                    fromLvl8:{
                        successCount:1,
                        minimalDiceScore:1,
                    },
                    fromLvl10:{
                        successCount:1,
                        minimalDiceScore:1,
                    },
                }
            },
            heal:{
                immune:{tension:false},
                learn:{
                    unlockTurn:1,
                    successCount:1,
                },
            },
            healed:{
                immune:{tension:true},
            },
            selfCare:{
                learn:{
                    unlockTurn:1,
                    successCount:1,
                },
            }
        }
    ]
}
const ai = {    defaults: prodAndBoost,
    alt:[{name:'defaultAI'},
        opnb,
        prodAndBoost,
        halfSupport,
        quartSupport,
    ]
}
let activeRules = getRules();
let activeAI = getAI();
export {activeAI,activeRules};



function deepMerge(base, alt) {
    if(typeof base !== 'object')
        return alt;
    const res = {...{}, ...base};
    for(const [k, v] of Object.entries(alt)) {
        res[k] = deepMerge(base[k], v);
    }
    return res;
}
function getMergedThing(thing,alt=0){
    return deepMerge(thing.defaults, thing.alt[alt])
}
export function getRules(alt=0) {
    return getMergedThing(gameVariants,alt);
}
export function getAI(alt=0) {
    return getMergedThing(ai,alt);
}
export function useRules(alt) {
    activeRules = getRules(alt);
}
export function useAI(alt) {
    activeAI = getAI(alt);
}