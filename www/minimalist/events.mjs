
export class GameEvent{
    constructor(sender) {
        this.sender = sender;
        this.name = this.constructor.name;
    }

}
export class Prod extends GameEvent{
    constructor(sender,amount) {
        super(sender);
        this.qty = amount;
    }
}
export class Fail extends GameEvent{}
export class Burnout extends GameEvent{}
export class Recovery extends GameEvent{}
export class Win extends GameEvent{
    constructor(sender) {
        super(sender);
        this.turn = sender.turn;
        this.size = sender.players.length;
        this.burnouts = sender.burnouts;
        this.history = sender.history;
        this.debts = sender.debts();
    }
}
export class Defeat extends GameEvent{
    constructor(sender,cause) {
        super(sender);
        this.turn = sender.turn;
        this.size = sender.players.length;
        this.cause = cause;
        this.history = sender.history;
    }
}
