import test from 'ava';
import * as app from './PubSub.mjs';
import {methodWrapper} from "./PubSub.mjs";

/*** PubSub class ***/
test('new PubSub() is an object with publish and addFollower method', t=>{
    const obj = new app.PubSub();
    t.is(typeof obj.addFollower, 'function');
    t.is(typeof obj.publish, 'function');
});
test(`this.eventRouter(ev) call this.when<ev.name>(ev) to handle the event`,t=>{
    t.plan(1)
    const obj = new app.PubSub();
    obj.whenDemo = ()=> t.pass();
    obj.eventRouter({name:'Demo'});
});
test(`if addFollower receive an object and not a function, it call the eventRouter of the object as eventHandler`,t=>{
    t.plan(1)
    const emitter = new app.PubSub();
    const consumer = new app.PubSub();
    consumer.whenDemo = ()=> t.pass();
    emitter.addFollower(consumer)

    emitter.publish({name:'Demo'});
});
test(`subscribe is an alias for addFollower`,t=>{
    t.plan(1)
    const emitter = new app.PubSub();
    const consumerCallBack = (ev)=>t.is(ev,'anything');
    emitter.subscribe(consumerCallBack)
    emitter.publish('anything');
});
/*** addPubSub(obj) ***/
test('addPubSub(obj) return obj', t=>{
    const obj = {};
    const res = app.addPubSub(obj);
    t.is(res, obj);
});
test('addPubSub(obj) add publish and subscribe method to obj', t=>{
    const obj = {};
    app.addPubSub(obj);
    t.is(typeof obj.addFollower, 'function');
    t.is(typeof obj.publish, 'function');
});
test('addPubSub(obj) allow to receive message after subscribe', t=>{
    t.plan(1);
    const messageHandler = (ev)=>t.is(ev,'anything');

    const obj = {};
    app.addPubSub(obj);
    obj.addFollower(messageHandler);
    obj.publish('anything');
});
test(`a custom methodWrapper can be used to trigger custom method on event publish`,t=>{
    t.plan(1)
    const emitter = new app.PubSub();
    const consumer = {custom:()=> t.pass()};
    emitter.addFollower(methodWrapper(consumer,'custom'));

    emitter.publish({name:'Demo'});

})